/*******************************************************************************
 * Package: com.song.rocket
 * Type:    UserConsumerListener
 * Date:    2024-03-27 14:06
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.rocket;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 功能描述：获取user-topic 里面的数据
 *
 * @author Songxianyang
 * @date 2024-03-27 14:06
 */
@RocketMQMessageListener(topic = "user-topic",selectorExpression = "tag1",consumerGroup = "my-consumer-rocket-group")
@Component
@Slf4j
public class UserConsumerListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        log.info("用户服务的topic接受的消息为：：：：：：{}", s);
    }
}
