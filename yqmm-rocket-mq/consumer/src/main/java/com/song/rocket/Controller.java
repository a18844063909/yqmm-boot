/*******************************************************************************
 * Package: com.song.rabbit.mq.web
 * Type:    MqController
 * Date:    2023-05-14 18:52
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.rocket;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-05-14 18:52
 */
@RestController
@RequestMapping("rocket-consumer")
public class Controller {

    @GetMapping("test")
    public String send() {
        return "success";
    }
}
