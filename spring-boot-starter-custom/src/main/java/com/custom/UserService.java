/*******************************************************************************
 * Package: com.custom
 * Type:    UserService
 * Date:    2024-04-26 11:49
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.custom;

/**
 * 功能描述： 用户组件
 * 自动配置文档
 *  https://www.cnblogs.com/hjwublog/p/10335464.html
 * @author Songxianyang
 * @date 2024-04-26 11:49
 */
public class UserService {
    private String name;
    private String age;
    private Long id;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void pint() {
        System.out.println(String.format("获取的数据用户名：%s----密码：%s----id:%s", name, age, id));
    }
}
