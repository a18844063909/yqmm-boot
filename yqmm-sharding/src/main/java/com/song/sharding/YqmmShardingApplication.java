package com.song.sharding;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("com.song.sharding.dao")
@ComponentScan(basePackages = {"com.song.common","com.song.sharding"})
public class YqmmShardingApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(YqmmShardingApplication.class, args);
    }
    
}
