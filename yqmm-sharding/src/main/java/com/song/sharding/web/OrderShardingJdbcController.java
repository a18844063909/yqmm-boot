package com.song.sharding.web;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.song.sharding.dao.IIntEndScaleForecastMapper;
import com.song.sharding.dao.IOrderShardingJdbcMapper;
import com.song.sharding.entity.IntEndScaleForecastEntity;
import com.song.sharding.entity.OrderShardingJdbcDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author SongXianYang
 * https://blog.csdn.net/weixin_44823875/article/details/128364789
 */
@Slf4j
@Api(tags = "ShardingJdbc-分库分表")
@RestController
@RequestMapping("/orderShardingJdbc")
public class OrderShardingJdbcController {

    @Resource
    private IOrderShardingJdbcMapper orderShardingJdbcMapper;
    @Resource
    private IIntEndScaleForecastMapper intEndScaleForecastMapper;


    @GetMapping("insert")
    @ApiOperation("分片插入")
    public void insert() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        for (Integer integer : list) {
            OrderShardingJdbcDO orderShardingJdbcDO = new OrderShardingJdbcDO();
            orderShardingJdbcDO.setOrderId(integer);
            orderShardingJdbcDO.setName("测试嘿嘿" + integer);
            orderShardingJdbcDO.setUserId(integer);
            orderShardingJdbcMapper.insert(orderShardingJdbcDO);
        }
    }

    @GetMapping("update")
    @ApiOperation("分片修改")
    public void update() {
        OrderShardingJdbcDO orderShardingJdbcDO = new OrderShardingJdbcDO();
        orderShardingJdbcDO.setOrderId(1);
        orderShardingJdbcDO.setName("测试嘿嘿" + "查询");
        orderShardingJdbcDO.setUserId(88);
        orderShardingJdbcMapper.updateById(orderShardingJdbcDO);
    }

    @GetMapping("list")
    @ApiOperation("分片查询")
    public IPage<OrderShardingJdbcDO> list(Long page, long size) {
        IPage<OrderShardingJdbcDO> iPage = new Page<>(page, size);
        QueryWrapper<OrderShardingJdbcDO> queryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<OrderShardingJdbcDO> lambda = queryWrapper.lambda();

        return orderShardingJdbcMapper.selectPage(iPage, lambda);
    }

    @GetMapping("delete")
    @ApiOperation("分片删除")
    public void delete() {
        orderShardingJdbcMapper.deleteById(10);
    }

    /**
     * 全局表（广播）
     *
     * @param subProjectName
     * @return
     */
    @ApiOperation("全局表")
    @GetMapping("/insert-complete/{subProjectName}")
    public String insert(@PathVariable("subProjectName") String subProjectName) {
        IntEndScaleForecastEntity entity = new IntEndScaleForecastEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setSubProjectName(subProjectName);
        intEndScaleForecastMapper.insert(entity);
        return "成功";
    }

    /**
     * 读写分离(读）
     *
     * @param
     * @return
     */
    @ApiOperation("读写分离-分页查")
    @GetMapping("list-read-write")
    public IPage<IntEndScaleForecastEntity> listReadWrite(Long page, long size) {
        IPage<IntEndScaleForecastEntity> iPage = new Page<>(page, size);
        QueryWrapper<IntEndScaleForecastEntity> queryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<IntEndScaleForecastEntity> lambda = queryWrapper.lambda();
        return intEndScaleForecastMapper.selectPage(iPage, lambda);
    }

    /**
     * 读写分离(写）
     *
     * @param
     * @return
     */
    @ApiOperation("读写分离-写")
    @GetMapping("list-read-write-insert")
    public boolean listReadWriteInsert(String id) {
        IntEndScaleForecastEntity entity = new IntEndScaleForecastEntity();
        entity.setId(id);
        entity.setSubProjectName("项目" + id);
        intEndScaleForecastMapper.insert(entity);
        return true;
    }

}