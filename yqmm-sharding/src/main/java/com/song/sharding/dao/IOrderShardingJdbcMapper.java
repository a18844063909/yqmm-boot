package com.song.sharding.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.song.sharding.entity.OrderShardingJdbcDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author SongXianYang
 */
@Mapper
public interface IOrderShardingJdbcMapper extends BaseMapper<OrderShardingJdbcDO> {
}