package com.song.sharding.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@Data
@TableName("order_sharding_jdbc")
public class OrderShardingJdbcDO {
	private Integer orderId;
	private String name;
	private Integer userId;
	private String password;
}