## docker-compose配合Dockerfile使用


也就是在dockers-compose.yml文件中添加`build`  
指定一下我的Dockerfile文件的路径  
例如我的dockers-compose.yml文件在docker-compose文件夹下，而docker-compose文件夹与Dockerfile和项目的war包在同一级目录，也就是Dockerfile文件，在dockers-compose.yml文件的上一级  
那么就指定`context: ../`和`dockerfile: Dockerfile`  
以上`../`就是上一级目录的意思，`Dockerfile`就是Dockerfile文件的名称  
来看dockers-compose.yml文件  
![在这里插入图片描述](https://img-blog.csdnimg.cn/098f79b3f4cf4f5fb3afd9c25157b4f5.png)

```yml
version: '3.1'
services:
  mysql:
    restart: always
    build:
      context: ../                  # 指定dockerfile文件的所在路径  
      dockerfile: Dockerfile    # 指定Dockerfile文件名称 
    image: ssm:1.0.1             #自定义镜像名和版本号
    container_name: ssm
    ports:
      - 8080:8080
    environment:
      TZ: Asia/Shanghai
```

来写Dockerfile文件，注意没有后缀  
![在这里插入图片描述](https://img-blog.csdnimg.cn/e4aef29fdbd24a459c41df05a0599bc6.png)

来看位置  
![在这里插入图片描述](https://img-blog.csdnimg.cn/da9163b33ae744b089de5c512ceb35d7.png)  
![在这里插入图片描述](https://img-blog.csdnimg.cn/1510791db07444c6b3b0cd01fd8f2127.png)

* * *

全部拖拽到Xterm中：

![在这里插入图片描述](https://img-blog.csdnimg.cn/c71d248b08484f51878a82f83539daf9.png)

在/opt目录下**新建**docker\_ssm文件夹

![在这里插入图片描述](https://img-blog.csdnimg.cn/fc450db0219e4362a208861092642fde.png)

`mv`命令把docker-compose、Dockerfile、ssm.war全部**移动**到/opt/docker\_ssm目录下  
![在这里插入图片描述](https://img-blog.csdnimg.cn/80dffd80bed144e4a552dd98b58fdda7.png)  
接着`cd`到docker-compose文件夹下  
然后`docker-compose up -d`命令运行  
![在这里插入图片描述](https://img-blog.csdnimg.cn/cdf28cd8fbaf4b9d8231e2301e1d8b7c.png)  
回车，运行结果如下：  
![在这里插入图片描述](https://img-blog.csdnimg.cn/b044a2111dc94c05993b874e1da0433e.png)  
WARNING警告的意思是：  
可以直接启动基于docker-compose.yml以及Dockerfile文件构建的自定义镜像，使用以下命令：

```auto
docker-compose up -d
```

如果使用Dockerfile文件自定义的镜像不存在，会帮助我们构建出自定义镜像，如果自定义镜像已经存在，会直接运行这个自定义镜像

如果我们想重新构建，输入以下命令  
重新构建自定义镜像

```auto
docker-compose build
```

如果想在运行容器前,重新构建，输入以下命令

```auto
docker-compose up -d --build
```

ERROR错误的意思是：  
在docker环境上面，配置完docker-compose.yml 配置文件以后，在运行容器之前，我的docker容器里面就已经启动了mysql镜像，8080这个端口已经被占了，进行启动时就会报错。输入以下命令：  
`docker stop 容器id`，把mysql关闭就行

## 数据库

数据库的导入导出和之前一致就行

接着就可以在浏览器访问了  
http://192.168.10.8:8080/ssm/