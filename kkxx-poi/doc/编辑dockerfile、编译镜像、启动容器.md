# 万能dockerfile编写模板
```yml
FROM openjdk:11.0 as builder
WORKDIR application
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} application.jar
RUN java -Djarmode=layertools -jar application.jar extract

FROM openjdk:11.0
WORKDIR application
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/spring-boot-loader/ ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
```
# 文件上传到服务器（关键是在于路径）
![img4.png](img/img_4.png)

![img5.png](img/img_5.png)

# 构建build镜像
> docker build . --tag java-project-name
> 
> java-project-name 构建的镜像名称
## 例如
> docker build . --tag layered-jars

![img5.png](img/img_6.png)

# 启动或者运行容器
> docker run -d -it -p 8080:8080 java-project-name:latest

## 例如
> docker run -it -d -p 15000:15000 rsc-chat-ai:latest
> 
![img5.png](img/img_7.png)
# 查端口进程
netstat -lnp|grep 15000
# （大功告成）容器运行成功 请求吧！