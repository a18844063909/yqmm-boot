# 报错1
```
ERROR: "docker buildx build" requires exactly 1 argument.
See 'docker buildx build --help'.

Usage:  docker buildx build [OPTIONS] PATH | URL | -

Start a build

```

![img.png](img/img.png)

# 解决：  后面加个 **.**
构建镜像
docker build -f /home/project/Dockerfile -t kkxx-poi-image:test .


# 报错2 
```
docker +ERROR [4/4] COPY  报错
```
启动镜像
docker run -d --name kkxx-poi-name -p 9092:9091 kkxx-poi-image:test

![img_2.png](img/img_2.png)

# 解决：
重新编写：dockerFile
> COPY ./kkxx-poi-1.1-SNAPSHOT.jar app.jar
> 
> 那么kkxx-poi-1.1-SNAPSHOT.jar app.jar必须存在于与Dockerfile相同的目录中。

![img_1.png](img/img_1.png)