/*******************************************************************************
 * Package: com.song.kkxxpoi.controller
 * Type:    UserWeb
 * Date:    2022-08-12 22:33
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.controller;

import com.song.common.response.ResponseVO;
import com.song.kkxxpoi.entity.Cat;
import com.song.kkxxpoi.entity.IntEndScaleForecastEntity;
import com.song.kkxxpoi.service.IIntEndScaleForecastService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-08-12 22:33
 */
@Api(tags = "拦截器")
@RestController
@RequestMapping("interceptor")
@Slf4j
public class InterceptorWeb {
    @Resource
    private IIntEndScaleForecastService iIntEndScaleForecastService;

    /**
     * mybatis-拦截器
     * @param subProjectName
     * @return
     */
    
    @GetMapping("insert/{subProjectName}")
    public String insert(@PathVariable("subProjectName") String subProjectName) {
        IntEndScaleForecastEntity entity = new IntEndScaleForecastEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setSubProjectName(subProjectName);
        
        //entity.setCreateBy("sss");
        //entity.setCreateTime(new Date());
        //entity.setUpdateBy("ssss");
        //entity.setUpdateTime(new Date());
        
        iIntEndScaleForecastService.insert(entity);
        return "成功";
    }

    /**
     * webmvc拦截器---拦截
     */
    @GetMapping("yes-Interceptors")
    public ResponseVO<String> yes() {
        return ResponseVO.success("调试webmvc拦截器");
    }
    /**
     * webmvc拦截器---不拦截
     */
    @GetMapping("no-Interceptors")
    public ResponseVO<String> no() {
        return ResponseVO.success("调试webmvc拦截器--不拦截");
    }

    @ApiOperation("调试入参为时间")
    @PostMapping("test-params-date")
    public Cat getCat(@RequestBody Cat cat) {
        return new Cat(cat.getId(), cat.getName(), cat.getStartDate());
    }

}
