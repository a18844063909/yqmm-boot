/*******************************************************************************
 * Package: com.song.kkxxpoi.websocket
 * Type:    WebSocketConfig
 * Date:    2024-02-20 17:26
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.websocket;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * 功能描述：
 *websocket配置类
 * @author Songxianyang
 * @date 2024-02-20 17:26
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    /**
     * 配置信息代理
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        // 订阅Broker名称,接受消息用户必须以这个开头的路径才能收到消息
        config.enableSimpleBroker("/user", "/topic", "/queue");
        // 全局使用的消息前缀（客户端订阅路径上会体现出来），客户端主动发送消息会以这里配置的前缀访问 @MessageMapping 配置的路径
        config.setApplicationDestinationPrefixes("/app");
        // 点对点使用的订阅前缀（客户端订阅路径上会体现出来），不设置的话，默认也是/user/
        config.setUserDestinationPrefix("/user");
    }

    /**
     * 注册stomp的端点
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry  registry) {
        // 允许使用socketJs方式访问，endpointSang，允许跨域
        // 在网页上我们就可以通过这个链接
        // http://localhost:9091/endpointSang
        // 来和服务器的WebSocket连接
        registry.addEndpoint("/endpointSang")
                // .addInterceptors(new HttpSessionHandshakeInterceptor())
                .setAllowedOriginPatterns("*") // 允许跨域设置
                .withSockJS();
    }
}
