/*******************************************************************************
 * Package: com.song.kkxxpoi.websocket
 * Type:    WebSocketController
 * Date:    2024-02-20 17:28
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.websocket;

import com.song.common.annotation.ResponseInfoSkin;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述：
 *https://blog.csdn.net/ch999999999999999999/article/details/102635322/ 开发
 *
 * 帮助文档
 * https://blog.csdn.net/2302_77835532/article/details/131482052
 *
 * @author Songxianyang
 * @date 2024-02-20 17:28
 */
@Api(tags = "WebSocket接口")
@RestController
@Slf4j
@ResponseInfoSkin
@CrossOrigin

public class WebSocketController {
    @Autowired
    private SimpMessagingTemplate messageTemplate;

    /**
     * 广播
     * @param message
     * @return
     */
    @MessageMapping("/welcome")
    //将消息发送到指定目的地的注解   前端通过/topic/greetings请求路径来接受/welcome推送过来的消息
    @SendTo("/topic/greetings")
    public Map say(Map message) {
        log.info("前端推送过来的消息为：{}",message.get("chatType").toString());
        Map res = new HashMap();
        res.put("topic", message.get("chatType").toString());
        return res;
    }

    /**
     * 请求接口推送消息-（广播）
     * @param message
     */
    @GetMapping("/sentMes")
    public void sentMes(String message) {
        log.info(message + "=======");
        this.messageTemplate.convertAndSend("/queue/msg", message);
    }

    /**
     * 点对点通信
     * @param message
     */
    //@MessageMapping(value = "/point")
    //@SendToUser("/topic/point")
    //public String point(Map message) {
    //    log.info(message.get("test") + "******---------------");
    //    return "dd";
    //}

    /**
     * 点对点通信
     * @param message
     */
    @MessageMapping(value = "/points")
    public void point1(Map message) {
        log.info(message.get("name") + "******" );
        //发送消息给指定用户, 最后接受消息的路径会变成 /user/admin/queue/points
        messageTemplate.convertAndSendToUser("admin", "/queue/points", message);
    }
}
