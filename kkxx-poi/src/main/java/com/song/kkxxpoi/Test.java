/*******************************************************************************
 * Package: com.song.kkxxpoi
 * Type:    Test
 * Date:    2022-03-26 14:06
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-03-26 14:06
 */
public class Test {
    public static void main(String[] args) throws ParseException {
        String s = "2022年01月(实际)";
        System.out.println(s.substring(9, 11));
        String mm = s.substring(0, 4) + "-" + s.substring(5, 7);
        System.out.println(mm);
        Date date = strToDateLong(mm);
        System.out.println(date);
        Map<Integer, Integer> map = new HashMap<>(1);
        map.put(1, 2);
    
        Date startDate = strToDateLong("2021-1");
        Date endDate = strToDateLong("2023-1");
        //boolean b = belongCalendar(date, startDate, endDate);
        //System.out.println(b);
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            
            System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
            boolean b = belongCalendar(date, startDate, endDate,entry);
            
            System.out.println(b);
            System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
        }
    
    }
    
    /**
     * 字符串日期转为Date
     * @param strDate
     * @throws ParseException
     */
    public static Date strToDateLong(String strDate) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
        Date lendDate = formatter.parse(strDate);
        return lendDate;
    }
    
    
    /**
     * 判断时间是否在时间段内
     * @param nowTime
     * @param beginTime
     * @param endTime
     * @return
     */
    public static boolean belongCalendar(Date nowTime, Date beginTime, Date endTime,Map.Entry<Integer, Integer> entry) {
        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);
        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);
        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
        if (date.after(begin) && date.before(end)) {
            entry.setValue(202);
            return true;
        }else if(nowTime.compareTo(beginTime)==0 || nowTime.compareTo(endTime) == 0 ){
            entry.setValue(9999);
            return true;
        }else {
            return false;
        }
    }
    
    
    
    
   
}
