/*******************************************************************************
 * Package: com.song.kkxxpoi.service
 * Type:    TransactionalEnd
 * Date:    2022-06-24 21:25
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.service;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-06-24 21:25
 */
public interface TransactionalEnd {
    
    void test1();
    void test2();
    
    void test3();
}
