/*******************************************************************************
 * Package: com.song.kkxxpoi.service
 * Type:    IIntEndScaleFrecastService
 * Date:    2022-05-02 15:54
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.service;

import com.song.kkxxpoi.entity.IntEndScaleForecastEntity;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-05-02 15:54
 */
public interface IIntEndScaleForecastService {
    /**
     * 新增
     * @param entity
     * @return
     */
    int insert(IntEndScaleForecastEntity entity);
    
    /**
     * 修改
     * @param entity
     * @return
     */
    int update(IntEndScaleForecastEntity entity);

    String insertOrUpdate(IntEndScaleForecastEntity entity);
}
