/*******************************************************************************
 * Package: com.song.kkxxpoi.service.impl
 * Type:    TransactionalEndImpl
 * Date:    2022-06-24 21:25
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.service.impl;

import com.song.kkxxpoi.entity.IntEndScaleForecastEntity;
import com.song.kkxxpoi.mapper.IIntEndScaleForecastMapper;
import com.song.kkxxpoi.service.TransactionalEnd;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 功能描述：事务的失效场景
 *
 * @author Songxianyang
 * @date 2022-06-24 21:25
 */
@Service
public class TransactionalEndImpl implements TransactionalEnd {
    @Resource
    private IIntEndScaleForecastMapper iIntEndScaleForecastMapper;
    
    @Override
    @Transactional()
    public void test1() {
        test2();
        test3();
        test4();
    }
    
    @Override
    public void test2() {
        IntEndScaleForecastEntity entity = new IntEndScaleForecastEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setSubProjectName("项目1");
        iIntEndScaleForecastMapper.insertIgnoreNull(entity);
    }
    
    @Override
    public void test3() {
        IntEndScaleForecastEntity entity = new IntEndScaleForecastEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setSubProjectName("项目2");
        iIntEndScaleForecastMapper.insertIgnoreNull(entity);
    }
    
    @Transactional(rollbackFor = Exception.class)
    public final void test4() {
        IntEndScaleForecastEntity entity = new IntEndScaleForecastEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setSubProjectName("项目3");
        iIntEndScaleForecastMapper.insertIgnoreNull(entity);
    }
}
