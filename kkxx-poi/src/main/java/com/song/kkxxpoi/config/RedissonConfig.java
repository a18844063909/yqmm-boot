/*******************************************************************************
 * Package: com.song.kkxxpoi.config
 * Type:    RedissonConfig
 * Date:    2022-07-29 23:06
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.config;

import com.song.common.util.CommUtil;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-07-29 23:06
 */
@SpringBootConfiguration
public class RedissonConfig {

    @Resource
    private RedisProperties redisProperties;

    @Bean(destroyMethod="shutdown")
    public RedissonClient redisson() {
        Config config = new Config();
        SingleServerConfig singleServerConfig = config.useSingleServer().setAddress("redis://" + redisProperties.getHost() + ":6379");
        if (CommUtil.isNotEmpty(redisProperties.getPassword())) {
            singleServerConfig.setPassword(redisProperties.getPassword());
        }
        RedissonClient redissonClient = Redisson.create(config);
        return redissonClient;
    }
    
}
