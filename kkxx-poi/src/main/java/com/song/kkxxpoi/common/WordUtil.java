package com.song.kkxxpoi.common;

import cn.afterturn.easypoi.word.WordExportUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
 
import java.io.*;
import java.util.Map;
 
@Component
@Slf4j
public class WordUtil {
 
    /**
     * 导出word
     * 模版变量中变量格式：{{foo}}
     *
     * @param templatePath word模板地址
     * @param saveDir      word文档保存的路径
     * @param fileName     文件名
     * @param params       替换的参数
     */
    public String exportWord(String templatePath, String saveDir, String fileName, Map<String, Object> params) {
        Assert.notNull(templatePath, "模板路径不能为空");
        Assert.notNull(saveDir, "临时文件路径不能为空");
        Assert.notNull(fileName, "导出文件名不能为空");
        Assert.isTrue(fileName.endsWith(".docx"), "word导出请使用docx格式");
//        Assert.isTrue(fileName.endsWith(".doc"), "word导出请使用docx格式");
 
        if (!saveDir.endsWith("/")) {
            saveDir = saveDir + File.separator;
        }
 
        File dir = new File(saveDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String savePath = saveDir + fileName;
 
        try {
            XWPFDocument doc = WordExportUtil.exportWord07(templatePath, params);
            FileOutputStream fos = new FileOutputStream(savePath);
            doc.write(fos);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            log.error("exportWord方法出现问题", e);
            e.printStackTrace();
        }
        return savePath;
    }
 
}