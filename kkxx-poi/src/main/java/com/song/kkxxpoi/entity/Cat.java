/*******************************************************************************
 * Package: com.song.kkxxpoi.entity
 * Type:    Cat
 * Date:    2023-12-26 17:31
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-12-26 17:31
 */
@Data
@ApiModel("猫")
@AllArgsConstructor
public class Cat {
    /**
     * 任务编号
     */
    @ApiModelProperty("id")
    private String id;
    /**
     * 任务执行编号
     */
    @ApiModelProperty("名字")
    private String name;
    /**
     * 任务名称
     */
    @ApiModelProperty("开始时间")
    private Date startDate;

}
