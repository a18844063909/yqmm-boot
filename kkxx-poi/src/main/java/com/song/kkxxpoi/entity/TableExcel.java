/*******************************************************************************
 * Package: com.song.kkxxpoi.entity
 * Type:    TableExcel
 * Date:    2024-04-19 15:52
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.entity;

import lombok.Data;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-04-19 15:52
 */
@Data
public class TableExcel {
    private String t1;
    private String t2;
    private String t3;
    private String t4;
    private String t5;
    private String t6;
    private String t7;
    private String t8;
    private String t9;
    private String t10;
    private String t11;
    private String t12;
    private String t13;
    private String t14;
    private String id;
}


