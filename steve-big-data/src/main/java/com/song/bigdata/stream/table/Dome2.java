/*******************************************************************************
 * Package: com.song.bigdata.stream.table
 * Type:    Dome2
 * Date:    2022-11-01 21:08
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.bigdata.stream.table;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Schema;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * 功能描述：Table Api
 *
 * @author Songxianyang
 * @date 2022-11-01 21:08
 */
public class Dome2 {
    public static void main(String[] args) {
        // 创建Table环境
        EnvironmentSettings settings = EnvironmentSettings.newInstance()
                .inStreamingMode() // 流式执行
                .useAnyPlanner()
                .build();
        //
        TableEnvironment tableEnvironment = TableEnvironment.create(settings);
        String tableCreate = "create table tableCreateInt ("
                +
                " user_name String," +
                " age Int, " +
                ") with ( "+
                " 'connector'='filesystem',"+
                "'path'='file/user.txt',"+
                "'format'='csv'"+ ")";
        // 创建table表 输入表
        tableEnvironment.executeSql(tableCreate);
        String sqlSelect="select user_name from tableCreateInt where user_name= 'song'";
        // 查询表中的数据
        Table table = tableEnvironment.sqlQuery(sqlSelect);

        // 创建输出表
        String tableCreateOut = "create table tableCreateOut (" +
                " user_name String," +
                ") with ( "+
                "'connector'='',"+
                ")";
        tableEnvironment.executeSql(tableCreateOut);
        table.executeInsert(tableCreateOut).print();
        // 查询 sql
        //
        //创建输出表
        // 不用持续执行
        //
        //
    }
}
