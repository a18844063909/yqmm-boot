package com.song.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.song.common","com.song.es"})
public class YqmmESApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(YqmmESApplication.class, args);
    }
    
}
