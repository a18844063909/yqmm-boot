#  [redis 发布、订阅模式](https://blog.csdn.net/weixin_47410172/article/details/126545320)
> 发布订阅都是客户端。指定好 Channel（服务端来存储 相当于redis里面对应的key）
# 发布者
```xml
PUBLISH song "yaoyao"   
<!-- song:Channel\ yaoyao:发送过去的消息  -->
```
![img.png](img.png)
# 订阅者（监听发布者的信息）
```xml
SUBSCRIBE song  
<!--（Channel）来监听这个队列里面的数据-->
```
![img_1.png](img_1.png)

# [分布式锁](https://blog.51cto.com/u_15973676/6074590)
> 可以采用 setnx命令来实现。但是会存在问题  就是过期时间过了  业务还没执行完。
> 
> redission分布式锁给ReentrantLock 用法是相同的。
> 
> 唯独的区别就是在程序在执行过程中如果业务没有执行完，就会触发自动续命机制！
> 
> redisson设置一个key的默认过期时间为30s，每隔10s帮你把key的超时时间设置为30s

