/*******************************************************************************
 * Package: com.song.boot.springstudy.dome
 * Type:    Test1
 * Date:    2022-12-23 21:00
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.dome;

import java.util.Arrays;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-12-23 21:00
 */
public class Test1 implements Call {
   static int[] list = new int[10];
    static int size;
    public static void main(String[] args) {
        System.out.println(size++);
        System.out.println(size++);
        System.out.println(size++);

        list[size++] = 1;
        list[size++] = 2;
        System.out.println(Arrays.asList(list));
    }

}
