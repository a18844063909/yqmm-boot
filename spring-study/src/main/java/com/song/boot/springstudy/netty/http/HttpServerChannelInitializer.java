/*******************************************************************************
 * Package: com.song.boot.springstudy.netty.http
 * Type:    HttpServerChannelInitializer
 * Date:    2023-06-20 21:16
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.http;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-06-20 21:16
 */
public class HttpServerChannelInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel sc) throws Exception {
        // 获取管道
        ChannelPipeline pipeline = sc.pipeline();
        // 获取http解码器
        pipeline.addLast("获取http解码器",new HttpServerCodec());
        pipeline.addLast("业务处理器", new HttpServerHandler());

    }
}
