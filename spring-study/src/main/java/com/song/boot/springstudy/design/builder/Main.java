/*******************************************************************************
 * Package: com.song.boot.springstudy.design.builder
 * Type:    Main
 * Date:    2024-04-18 10:50
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.builder;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-04-18 10:50
 */
public class Main {
    public static void main(String[] args) {
        Cat build = new CatBuilder().idBuild(111l).nameBuild("松下演员").ageBuild(12).build();
        System.out.println(build);
    }
}
