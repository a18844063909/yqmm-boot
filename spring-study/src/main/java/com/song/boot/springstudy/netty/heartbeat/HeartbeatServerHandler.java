/*******************************************************************************
 * Package: com.song.boot.springstudy.netty.heartbeat
 * Type:    HeartbeatServerHandler
 * Date:    2023-06-24 14:59
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.heartbeat;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-06-24 14:59
 */
public class HeartbeatServerHandler extends ChannelInboundHandlerAdapter {
    /**
     * 心跳检测方法
     * @param ctx
     * @param evt 事件
     * @throws Exception
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent idleStateEvent= (IdleStateEvent) evt;
            String evtType = null;
            switch (idleStateEvent.state()) {
                case READER_IDLE:
                    evtType = "读空闲";
                    break;
                case WRITER_IDLE:
                    evtType="写空闲";
                    break;
                case ALL_IDLE:
                    evtType = "读写空闲";
            }
            System.out.println("到底谁空闲：：：》》》》" + evtType);
        }
    }
}
