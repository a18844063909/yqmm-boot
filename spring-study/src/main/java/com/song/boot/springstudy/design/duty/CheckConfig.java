/*******************************************************************************
 * Package: com.song.boot.springstudy.design.duty
 * Type:    CheckConfig
 * Date:    2022-12-29 23:31
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.duty;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 功能描述：
 *sort
 * @author Songxianyang
 * @date 2022-12-29 23:31
 */
@Data
@Component
@ConfigurationProperties(prefix = "check")
public class CheckConfig {
    // 处理规则顺序指定
    public List<String> sortBeanNames;
}
