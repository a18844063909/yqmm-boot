/*******************************************************************************
 * Package: com.song.boot.springstudy
 * Type:    Test3
 * Date:    2022-06-28 12:38
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-06-28 12:38
 */
public class Test3 {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(4);
        integers.add(5);
        integers.add(6);
        List<Integer> news = new ArrayList<>();
        // 删除那些没有应还本金的数据
        Iterator<Integer> iterator = integers.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            i++;
            if (i <= 3) {
                news.add(iterator.next());
            } else {
                break;
            }
        }
        integers.clear();
        integers.addAll(news);
        System.out.println(integers);
    }
}
