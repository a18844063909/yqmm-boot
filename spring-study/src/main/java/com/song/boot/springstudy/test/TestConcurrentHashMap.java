/*******************************************************************************
 * Package: com.song.boot.springstudy.test
 * Type:    TestCollect
 * Date:    2022-12-03 14:51
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 功能描述：
 * map.put()代码底层3
 * @author Songxianyang
 * @date 2022-12-03 14:51
 */
public class TestConcurrentHashMap {
    public static void main(String[] args) {
        Map<Integer, String> map = new ConcurrentHashMap<>();
        map.put(99,"测试");
        for (int i = 0, size = 14; i < size; i++) {
            map.put(i, "song:"+(i+1));
        }
        map.put(1, "嘿嘿");
        System.out.println(map);
    }
}
