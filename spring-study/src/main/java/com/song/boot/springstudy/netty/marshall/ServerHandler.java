/*******************************************************************************
 * Package: com.song.boot.springstudy.marshall
 * Type:    ServerHandler
 * Date:    2024-01-28 16:18
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.marshall;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-01-28 16:18
 */
public class ServerHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        // 接受客户端信息
        ReqData reqData=  (ReqData)msg;
        System.out.println("接收到客户端消息id"+reqData.getId());
        System.out.println("接收到客户端消息Name"+reqData.getName());
        System.out.println("接收到客户端消息RequestMessage"+reqData.getRequestMessage());

        // 响应客户端 我接受到了消息
        RespData data = new RespData();
        data.setId(reqData.getId());
        data.setName(reqData.getName());
        data.setRequestMessage("我接受到了消息");
        ctx.writeAndFlush(data);
    }
    // 发生异常，则关闭通道
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}
