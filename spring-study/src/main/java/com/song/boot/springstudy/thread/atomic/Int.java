/*******************************************************************************
 * Package: com.song.boot.springstudy.thread.atomic
 * Type:    Int
 * Date:    2022-06-14 21:47
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 功能描述：AtomicInteger 保证原子性
 *
 * @author Songxianyang
 * @date 2022-06-14 21:47
 */
public class Int {
    /**
     * 原子整型
     */
    private static AtomicInteger atomicInteger = new AtomicInteger();
    private static volatile int i;
    
    public static void main(String[] args) throws InterruptedException {
        Int anInt = new Int();
        Thread thread = new Thread(() -> {
            anInt.test();
        });
        Thread thread1 = new Thread(() -> {
            anInt.test();
        });
        thread.start();
        thread1.start();
        thread1.join();
        thread.join();
    
        System.out.println("原子int 加加结果："+atomicInteger.get());
        System.out.println("没有原子操作的i："+ i);
    }
    
    private void test() {
        for (int j = 0, size = 1000; j < size; j++) {
            atomicInteger.getAndIncrement();
            i++;
        }
    }
}
