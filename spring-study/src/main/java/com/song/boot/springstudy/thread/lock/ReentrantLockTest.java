/*******************************************************************************
 * Package: com.song.boot.springstudy.thread.lock
 * Type:    ReentrantLockTest
 * Date:    2022-06-04 22:29
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 功能描述： 一个线程不断的去重入的过程  也是递归的过程
 *
 * @author Songxianyang
 * @date 2022-06-04 22:29
 */
public class ReentrantLockTest {
    private static ReentrantLock lock = new ReentrantLock();
    
    public static void main(String[] args) {
        System.out.println(lock.getHoldCount());
        lock.lock();
        System.out.println(lock.getHoldCount());
        lock.lock();
        System.out.println(lock.getHoldCount());
        lock.lock();
        System.out.println(lock.getHoldCount());
        System.out.println("-----------");
        lock.unlock();
        System.out.println(lock.getHoldCount());
        lock.unlock();
        System.out.println(lock.getHoldCount());
        lock.unlock();
        System.out.println(lock.getHoldCount());
    }
}
