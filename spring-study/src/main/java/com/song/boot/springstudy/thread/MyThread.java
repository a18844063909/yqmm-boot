/*******************************************************************************
 * Package: com.song.boot.springstudy.thread
 * Type:    MyThread
 * Date:    2023-02-15 21:58
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-02-15 21:58
 */
public class MyThread {
    public static void main(String[] args) throws InterruptedException {
        Thread parentParent = new Thread(() -> {
            ThreadLocal<Integer> threadLocal = new ThreadLocal<>();
            threadLocal.set(1);
            InheritableThreadLocal<Integer> inheritableThreadLocal = new InheritableThreadLocal<>();
            inheritableThreadLocal.set(2);

            new Thread(() -> {
                System.out.println("threadLocal=" + threadLocal.get());
                System.out.println("inheritableThreadLocal=" + inheritableThreadLocal.get());
            }).start();
        }, "父线程");
        parentParent.start();
    }
}
