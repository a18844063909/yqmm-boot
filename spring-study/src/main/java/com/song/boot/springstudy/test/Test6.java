/*******************************************************************************
 * Package: com.song.boot.springstudy
 * Type:    Test6
 * Date:    2022-11-07 22:35
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-11-07 22:35
 */
public class Test6 {
    public static void main(String[] args) {
        System.out.println(extracted(0));
    }

    private static int extracted(int i) {
        System.out.println(i);
        try {
            i++;
            return i;
        }finally {
            i++;
        }
    }
}
