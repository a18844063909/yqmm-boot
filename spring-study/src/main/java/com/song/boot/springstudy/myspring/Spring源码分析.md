# [学习视频](https://www.bilibili.com/video/BV1tR4y1F75R)
# bean创建：DI控制反转
> 扫描路径：ComponentScan（com.song.boot.springstudy）所有类
    
> 是否标记有：Component注解
    
> 有就创建bean，（单例还是多例bean）

> BeanDefinition 封装所有bean名称 、类型、是否单例

> 通过map容器来存放bean<beanName,BeanDefinition>。单例bean也用一个**单例池**来存放<beanName,BeanDefinition单例的>   ,通过是否单例这个字段来处理。

> 没有跳过

# 依赖注入
> 意思：在创建bean的时候 ：看一下当前bean中属性上是否标有Autowired这个注解

> 如果有这个注解：通过反射获取 获取属性.getName。赋值给当前对象当前对象的属性。如：class.set(当前bean，获取属性.getName)
# bean生命周期：
> 创建bean

> 属性赋值

> 执行回调Aware (bean对象 instance **Aware) 赋值逻辑

> 初始化当前bean 数据 (bean对象 instance 任意) 初始化逻辑（在spring运行的时候初始化一些数据）

> BeanPostProcessor 两个方法：bean对象初始化前  和初始化后执行的方法（生成代理对象，AOP执行切面逻辑）
## 整体梳理bean生命周期：
> 创建bean(反射无参的构造法方法创建bean)--->属性赋值（Autowired，依赖注入）--->_可能会有回调_-->初始化前（@PostConstruct注解方法）---> 初始化(InitializingBean这个对象里面的方法)--->初始化后--->可能会有_aop_--->单例池--->bean对象

> 构造方法：优先无参构造方法   其次有参构造方法（先byType,再byName)
> 
> 属性赋值 ：构造方法注入 和Autowired 都是（先byType,再byName）
> 
> aop的实现：生成代理对象，里面附加原生bean对象中的属性。从而最终使用的流程：执行切面的时候是代理对象，执行bean对象里面的方法的时候是代理对象。
````java
class UserServiceProxy extends UserService {
    UserService target;
    public void test() {
        // 切面逻辑@Before
        // target.test();
        target.test();
    }
}
````
> Spring中事务是怎么控制的：AOP
> 关闭事务自动提交 --> 在当前的Service 调用其他增删改的时候 自己调用自己的时候是不生效的。因为不是使用代理对象。

**如何解决:**
> 1、再创建一个beanBase对象对应增删改的方法带上@Transactional注解
> 
> 然后当前的Service 注入beanBase从而调用增删改
> 
> 2、当前的Service 自己注入自己  从而调用增删改 
> 
> 以上两种使用的都是代理对象是生效的。
```java
// AOP 大概底层 方法上标有Transactional注解
class UserServiceProxy extends UserService {
    UserService target;
    public void test() {
        // Spring事务的切面逻辑
        //开始事务
        // 创建数据库连接
        conn.autoCommit=false;
        // 执行代理方法 sql1 sql2
        target.test();
        conn.commit();
        conn.rollback();
    }
}
```
**@Configuration含义：**
> 如果Spring容器中有对应的bean对象  就不会去创建 就直接用。如果不加 就会创建多个bean对象就无法实现事务管理：自动提交。从而控制数据库连接是同一个才可以控制事务。
> 
> Transactional主要用的是：ThreadLocal<Map<DataSource,conn>>  DataSource是同一个即可。

# 为啥Spring的bean都是单例的？单例池存放的是代理对象还是非代理对象
> 因为spring在启动的时候会用一个单例池来存放来存放单例bean。
