/*******************************************************************************
 * Package: com.song.boot.springstudy.marshall
 * Type:    Client
 * Date:    2024-01-28 16:21
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.marshall;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-01-28 16:21
 */
public class Client {
    public static void main(String[] args) throws InterruptedException {
        EventLoopGroup workerEvent = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(workerEvent)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG,128) // 设置线程队列
        .handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                socketChannel.pipeline().addLast(MarshallingCodec.buildMarshallingEncoder());
                socketChannel.pipeline().addLast(MarshallingCodec.buildMarshallingDecoder());
                socketChannel.pipeline().addLast(new ClientHandler()); // 自定义的server处理器
            }
        });
        ChannelFuture connect = bootstrap.connect("127.0.0.1", 9999).sync();

        connect.channel().closeFuture().sync();

        workerEvent.shutdownGracefully();  // 关闭
    }
}
