/*******************************************************************************
 * Package: com.song.boot.springstudy
 * Type:    C
 * Date:    2023-01-02 15:54
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design;

import java.util.Arrays;
import java.util.Objects;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-01-02 15:54
 */
public class C {
    public static void main(String[] args) {
        A b = new B();
        //b.del();
        b.insert();

        System.out.println(Arrays.asList("1", "2").add("3"));
    }

}
