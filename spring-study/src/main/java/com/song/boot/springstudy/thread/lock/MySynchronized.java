/*******************************************************************************
 * Package: com.song.boot.springstudy.thread.lock
 * Type:    MySynchronized
 * Date:    2023-08-23 17:06
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread.lock;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-08-23 17:06
 */
public class MySynchronized {
    public static void main(String[] args) {
        synchronized (MySynchronized.class) {
            System.out.println("1112");
        }
    }

    private synchronized static void test() {
        System.out.println("00000000");
    }
}
