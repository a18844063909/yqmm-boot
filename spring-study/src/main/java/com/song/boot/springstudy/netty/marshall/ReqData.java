/*******************************************************************************
 * Package: com.song.boot.springstudy.marshall
 * Type:    RequestData
 * Date:    2024-01-28 16:00
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.marshall;

import lombok.Data;

import java.io.Serializable;

/**
 * 功能描述：Client发送到Server请求数据
 *
 * @author Songxianyang
 * @date 2024-01-28 16:00
 */
@Data
public class ReqData  implements Serializable {
    private String id;
    private String name;
    private String requestMessage;
    //    也可以发送二进制文件
    //byte[] bytes;
}
