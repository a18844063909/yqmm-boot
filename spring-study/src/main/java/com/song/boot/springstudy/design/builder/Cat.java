/*******************************************************************************
 * Package: com.song.boot.springstudy.design.builder
 * Type:    Cat
 * Date:    2024-04-18 10:02
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.builder;

import lombok.Data;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-04-18 10:02
 */
@Data
public class Cat implements Serializable {
    private static final long serialVersionUID = -1689622901194791639L;
    private String name;
    private Integer age;
    private Long id;

    public Cat(String name, Integer age, Long id) {
        this.name = name;
        this.age = age;
        this.id = id;
    }
}
