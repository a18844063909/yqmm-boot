/*******************************************************************************
 * Package: com.song.boot.springstudy.netty.proto
 * Type:    TestMain
 * Date:    2024-01-30 10:23
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.proto;

import com.song.boot.springstudy.netty.proto.protoDoMain.CatInfo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-01-30 10:23
 */
public class TestMain {

    public static void main(String[] args) throws Exception {
        // 创建一个对象
        CatInfo.CatMsg.Builder builder = CatInfo.CatMsg.newBuilder();
        builder.setId(11);
        builder.setAge(20);
        builder.setName("song");
        builder.setState(0);

        CatInfo.CatMsg catMsg = builder.build();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        // 将数据写到输出流
        catMsg.writeTo(output);
        // 将数据序列化后发送
        byte[] bytes = output.toByteArray();
        // 接收到流并读取
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        // 将数据反序列化后打印在控制台
        CatInfo.CatMsg catMsgEntity = CatInfo.CatMsg.parseFrom(inputStream);
        System.out.println(catMsgEntity.getAge());
        System.out.println(catMsgEntity.getId());
        System.out.println(catMsgEntity.getName());
        System.out.println(catMsgEntity.getState());
        inputStream.close();
        output.close();
    }
}
