/*******************************************************************************
 * Package: com.song.boot.springstudy.design
 * Type:    A
 * Date:    2022-12-29 23:11
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design;

/**
 * 功能描述： 实现类并不需要为这些方法提供功能定义。
 *
 * @author Songxianyang
 * @date 2022-12-29 23:11
 */
public abstract class A {
    public void del() {
        // 实现类并不需要为这些方法提供功能定义
        throw new UnsupportedOperationException("出错了");
    }
    public abstract void insert();
}
