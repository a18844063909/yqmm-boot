/*******************************************************************************
 * Package: com.song.boot.springstudy.entity
 * Type:    One
 * Date:    2022-10-31 10:04
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.entity;

import lombok.Data;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-10-31 10:04
 */
@Data
public class One implements Cloneable {
    private String count;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
