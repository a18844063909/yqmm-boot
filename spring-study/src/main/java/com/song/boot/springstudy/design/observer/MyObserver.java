/*******************************************************************************
 * Package: com.song.boot.springstudy.design.observer
 * Type:    MyObserver
 * Date:    2023-01-02 21:57
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.observer;

/**
 * 功能描述：观察者模式：
 *  角色1：观察者--微信用户抽象---订阅者
 * 帮助文档：
 *  https://juejin.cn/post/6844903839900909575
 *  https://blog.csdn.net/NiKaBoy/article/details/127377641
 *  https://blog.csdn.net/leilifengxingmw/article/details/74139745 推荐
 * @author Songxianyang
 * @date 2023-01-02 21:57
 */
public interface MyObserver{
    /**
     * 通知
     * @param msg
     */
    void accept(String msg);
}
