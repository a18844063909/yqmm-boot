/*******************************************************************************
 * Package: com.song.boot.springstudy.test
 * Type:    TestCollect
 * Date:    2022-12-03 14:51
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

import org.apache.http.util.Asserts;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述：
 * map.put()代码底层3
 * resize 扩容源码底层4
 * 什么时候发起扩容如何扩容3   threshold  默认参数  下   size > threshold=12
 * 什么时候转红黑树5  链表长度大于7   binCount >= 8 - 1 &&tab.length > 64
 * 初始化数据的时机3 resize（）
 * transient 什么意思 1
 * modCount 用这个有什么用2
 * 为什么转红黑树6
 * -什么是序列化和反序列化
 * @author Songxianyang
 * @date 2022-12-03 14:51
 */
public class TestCollect {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(99,"测试");
        for (int i = 0, size = 14; i < size; i++) {
            map.put(i, "song:"+(i+1));
        }
        map.put(1, "嘿嘿");
        System.out.println(map);
    }
}
