/*******************************************************************************
 * Package: com.song.boot.springstudy.myspring.bean
 * Type:    Cat
 * Date:    2022-05-13 15:42
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.myspring.bean;

import lombok.Data;

/**
 * 功能描述：bean 对象
 *
 * @author Songxianyang
 * @date 2022-05-13 15:42
 */
@Data
public class CatBean {
    private String name;
}
