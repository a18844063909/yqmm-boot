/*******************************************************************************
 * Package: com.song.boot.springstudy.design
 * Type:    B
 * Date:    2023-01-02 15:52
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-01-02 15:52
 */
public class B extends A{
    @Override
    public void insert() {
        System.out.println("插入");
    }
}
