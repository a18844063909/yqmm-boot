/*******************************************************************************
 * Package: com.song.boot.springstudy.kd
 * Type:    QzapiImpl
 * Date:    2023-06-18 18:36
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.kd;

import net.sf.json.JSONObject;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-06-18 18:36
 */

public class QzapiImpl {

    private String xh = "xxxx";
    private String pwd = "xxxx";
    private List<BasicNameValuePair> list;

    private String token;
    private Util util = null;

    public QzapiImpl() {
        list = new ArrayList<BasicNameValuePair>();
        util = new Util();
        try {
            token = util.getToken(xh, pwd);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public JSONObject authUser(String xh, String pwd) {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","authUser"));
        list.add(new BasicNameValuePair("xh",xh));
        list.add(new BasicNameValuePair("pwd",pwd));
        return util.execute(list,token);
    }

    public JSONObject getCurrentTime(String currDate) {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","authUser"));
        list.add(new BasicNameValuePair("currDate",currDate));
        return util.execute(list,token);
    }

    public JSONObject getKbcxAzc(String xh, String xnxqid, String zc) {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","getKbcxAzc"));
        list.add(new BasicNameValuePair("xh",xh));
        list.add(new BasicNameValuePair("xnxqid",xnxqid));
        list.add(new BasicNameValuePair("zc",zc));
        return util.execute(list,token);
    }

    public JSONObject getCjcx(String xh, String xnxqid) {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","getCjcx"));
        list.add(new BasicNameValuePair("xh",xh));
        list.add(new BasicNameValuePair("xnxqid",xnxqid));
        return util.execute(list,token);
    }

    public JSONObject getKscx(String xh) {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","getKscx"));
        list.add(new BasicNameValuePair("xh",xh));
        return util.execute(list,token);
    }

    public JSONObject getJxlcx(String xqid) {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","getJxlcx"));
        list.add(new BasicNameValuePair("xqid",xqid));
        return util.execute(list,token);
    }

    public JSONObject getKxJscx(String time, String idleTime, String xqid, String jxlid, String classroomNumber) {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","getKxJscx"));
        list.add(new BasicNameValuePair("time",time));
        list.add(new BasicNameValuePair("idleTime",idleTime));
        list.add(new BasicNameValuePair("xqid",xqid));
        list.add(new BasicNameValuePair("jxlid",jxlid));
        list.add(new BasicNameValuePair("classroomNumber",classroomNumber));
        return util.execute(list,token);
    }

    public JSONObject getStudentIdInfo(String xh) {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","getStudentIdInfo"));
        list.add(new BasicNameValuePair("xh",xh));
        return util.execute(list,token);
    }

    public JSONObject getUserInfo(String xh) {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","getUserInfo"));
        list.add(new BasicNameValuePair("xh",xh));
        return util.execute(list,token);
    }

    public JSONObject getXnxq(String xh) {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","getXnxq"));
        list.add(new BasicNameValuePair("xh",xh));
        return util.execute(list,token);
    }

    public JSONObject getXqcx() {
        list.clear();  //从列表中删除所有元素。
        list.add(new BasicNameValuePair("method","getXqcx"));
        return  util.execute(list,token);
    }

}
