/*******************************************************************************
 * Package: com.song.boot.springstudy.design.observer
 * Type:    WxUser
 * Date:    2023-01-05 21:24
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.observer;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-01-05 21:24
 */
public class WxUser extends User implements MyObserver{


    public WxUser(String name) {
        super(name);
    }

    @Override
    public void accept(String msg) {
        System.out.println(this.getName()+":接受到公众号群发的消息为："+msg);
    }
}
