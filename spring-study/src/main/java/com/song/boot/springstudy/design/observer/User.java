/*******************************************************************************
 * Package: com.song.boot.springstudy.design.observer
 * Type:    User
 * Date:    2023-01-05 21:21
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.observer;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 功能描述：用户信息
 *
 * @author Songxianyang
 * @date 2023-01-05 21:21
 */
@Data
@AllArgsConstructor
public class User {
    // 用户名
    private String name;
    // 社区名
    //private String communityName;
}
