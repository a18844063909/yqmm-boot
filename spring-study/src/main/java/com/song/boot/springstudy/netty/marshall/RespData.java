/*******************************************************************************
 * Package: com.song.boot.springstudy.marshall
 * Type:    RespData
 * Date:    2024-01-28 16:02
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.marshall;

import lombok.Data;

import java.io.Serializable;

/**
 * 功能描述：Server发送到Client响应数据
 *
 * @author Songxianyang
 * @date 2024-01-28 16:02
 */
@Data
public class RespData implements Serializable {
    private String id;
    private String name;
    private String requestMessage;
}
