/*******************************************************************************
 * Package: com.song.flowable.mapper
 * Type:    IActDeModelMapper
 * Date:    2023-11-30 18:12
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.song.flowable.entity.ActDeModelDO;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-30 18:12
 */
public interface IActDeModelMapper extends BaseMapper<ActDeModelDO> {
}
