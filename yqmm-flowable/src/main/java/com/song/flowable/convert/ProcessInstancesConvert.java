/*******************************************************************************
 * Package: cn.surveyking.module.flowable.convert
 * Type:    ProcessInstancesConvert
 * Date:    2023-12-26 9:43
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.convert;


import com.song.flowable.vo.ProcessInstancesVO;
import org.flowable.engine.history.HistoricProcessInstance;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-12-26 9:43
 */
@Mapper
public interface ProcessInstancesConvert {
    ProcessInstancesConvert INSTANCE = Mappers.getMapper(ProcessInstancesConvert.class);
    @Mappings({
            @Mapping(source = "id", target = "processInstanceId"),
    })
    ProcessInstancesVO toVo(HistoricProcessInstance source)  ;

    List<ProcessInstancesVO> toVoList(List<HistoricProcessInstance> source);

}
