/*******************************************************************************
 * Package: com.song.flowable.vo
 * Type:    ProcessDefinitionVO
 * Date:    2023-11-29 16:48
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.flowable.engine.FormService;

/**
 * 功能描述：流程定义VO
 *
 * @author Songxianyang
 * @date 2023-11-29 16:48
 */
@ApiModel("反参流程定义VO")
@Data
public class ProcessDefinitionVO {
    /**
     * unique identifier
     */
    private String id;

    /**
     * category name which is derived from the tarNamespace attribute in the definitions element
     */
    private String category;

    /**
     * label used for display purposes
     */
    private String name;

    /**
     * unique name for all versions this process definitions
     */
    private String key;

    /**
     * description of this process
     **/
    private String description;

    /**
     * version of this process definition
     */
    private int version;

    /**
     * of this process definition.
     */
    private String resourceName;

    /**
     * The deployment in which this process definition is contained.
     */
    private String deploymentId;

    /**
     * The resource name in the deployment of the diagram image (if any).
     */
    private String diagramResourceName;


    /**
     * The tenant identifier of this process definition
     */
    private String tenantId;

    /**
     * The derived from process definition value when this is a dynamic process definition
     */
    private String derivedFrom;

    /**
     * The root derived from process definition value when this is a dynamic process definition
     */
    private String derivedFromRoot;

    /**
     * The derived version of the process definition
     */
    private int derivedVersion;

    /**
     * The engine version for this process definition (5 or 6)
     */
    private String engineVersion;
}
