/*******************************************************************************
 * Package: com.song.flowable.vo
 * Type:    ProcessInstancesVO
 * Date:    2023-12-26 10:31
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-12-26 10:31
 */
@ApiModel("流程实例vo")
@Data
public class ProcessInstancesVO {
    @ApiModelProperty("流程实例id")
    private String processInstanceId;
    @ApiModelProperty("流程实例版本")
    private Integer revision;
    @ApiModelProperty("流程定义id")
    private String processDefinitionId;
    @ApiModelProperty("开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;
    @ApiModelProperty("结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;
    @ApiModelProperty("流程定义的名称")
    private String processDefinitionName;
    @ApiModelProperty("流程定义key")
    private String processDefinitionKey;
    @ApiModelProperty("流程部署id")
    private String deploymentId;
    @ApiModelProperty("流程终止原因")
    private String deleteReason;
}
