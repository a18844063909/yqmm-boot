package com.song.flowable.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel("更新模型状态")
@Data
public class ModelUpdateStateVO {

    @ApiModelProperty("模型id")
    private String id;
    @ApiModelProperty("状态")
    private Integer state;

}
