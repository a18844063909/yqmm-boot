/*******************************************************************************
 * Package: com.song.flowable.listener
 * Type:    SignApplyTaskListener
 * Date:    2022-09-16 21:51
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.listener;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述： 会签监听器
 *com.song.flowable.listener.SignApplyTaskListener
 * @author Songxianyang
 * @date 2022-09-16 21:51
 */
@Service
public class SignApplyTaskListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution execution) {
        Map<String, Integer> map = new HashMap<>(2);
        //拒绝人数
        map.put("reject", 0);
        //同意人数
        map.put("agree", 0);
        execution.setVariables(map);
    }
}
