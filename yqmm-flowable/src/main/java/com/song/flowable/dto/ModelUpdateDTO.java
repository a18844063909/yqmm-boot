/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    ModelUpdateDTO
 * Date:    2023-12-10 17:18
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述：修改流程模型DTO
 *
 * @author Songxianyang
 * @date 2023-12-10 17:18
 */
@Data
@ApiModel(value = "流程模型保存实体")
public class ModelUpdateDTO {
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "描述")
    private String description;
    @ApiModelProperty(value = "模型id")
    private String id;
    @ApiModelProperty(value = "流程xml")

    private String flowXml;
// TODO 如果有外置表单 记得在这里维护 字段
}
