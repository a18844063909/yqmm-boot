package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


import java.util.Date;

@ApiModel("获取流程模型")
@Data
public class ModelRespDTO {

    @ApiModelProperty("流程标识")
    private String key;
    @ApiModelProperty("流程名称")
    private String name;
    @ApiModelProperty("流程描述")
    private String description;
    @ApiModelProperty("流程分类")
    private String category;
    @ApiModelProperty("编号")
    private String id;
    @ApiModelProperty("xml")
    private String bpmnXml;
    @ApiModelProperty("创建时间")
    private Date createTime;

}
