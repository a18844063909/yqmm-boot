/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    FlowReturn
 * Date:    2023-11-19 15:55
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述： 流程退回
 *
 * @author Songxianyang
 * @date 2023-11-19 15:55
 */
@ApiModel("流程退回")
@Data
public class FlowReturnDTO {
    /**
     * 当前任务节点id
     * 也就是xml流程图上面的节点id
     * 代办任务的 nodeId
     */
    @ApiModelProperty("当前任务节点id")
    private String currentNodeTaskId;

    /**
     *目标任务节点id
     * 也就是xml流程图上面的节点id
     * 通过查询可退回的节点列表：/flow/task/get-return-task-list  获取
     */
    @ApiModelProperty("目标任务节点id")
    private String targetNodeTaskId;

    /**
     *流程实例id
     */
    @ApiModelProperty("流程实例id")
    private String processInstanceId;

    /**
     * 任务id
     */
    @ApiModelProperty("任务id")
    private String taskId;
    /**
     * 审批意见
     */
    @ApiModelProperty("审批意见")
    private String message;

    /**
     * 审批人id
     */
    @ApiModelProperty("审批人id")
    private String examineUserId;


}
