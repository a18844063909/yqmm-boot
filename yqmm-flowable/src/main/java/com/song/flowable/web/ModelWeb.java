/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    ModelWeb
 * Date:    2023-11-27 20:52
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.song.flowable.dto.FlowModelDTO;
import com.song.flowable.dto.ModelPageReqDTO;
import com.song.flowable.dto.ModelUpdateDTO;
import com.song.flowable.service.ModelFlowService;
import com.song.flowable.vo.ModelUpdateStateVO;
import com.song.flowable.vo.ModelVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.flowable.ui.modeler.model.ModelRepresentation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 功能描述：流程模型
 *
 * @author Songxianyang
 * @date 2023-11-27 20:52
 */
@RequiredArgsConstructor
@Api(tags = "流程模型")
@RestController
@RequestMapping("/flow/model")
public class ModelWeb {

    private final ModelFlowService modelFlowService;

    @PostMapping("/page")
    @ApiOperation("列表分页")
    public List<ModelVO> getModelPage(@RequestBody ModelPageReqDTO dto) {
        return modelFlowService.getModelPage(dto);
    }

    @GetMapping("/get")
    @ApiOperation("获得模型")
    public ModelVO getModel(@RequestParam("id") String id) {
        ModelVO model = modelFlowService.getModel(id);
        return model;
    }

    @PostMapping("/update-state")
    @ApiOperation("修改模型部署的流程定义状态")
    public String updateModelState(@RequestBody ModelUpdateStateVO vo) {
        return modelFlowService.updateModelState(vo.getId(), vo.getState());
    }

    @DeleteMapping("/delete")
    @ApiOperation( "删除模型")
    public Boolean deleteModel(@RequestParam("id") String id) {
        return modelFlowService.deleteModel(id);
    }


    /**
     * 添加流程模型
     *
     * @param req
     */

    @PostMapping("/model-insert")
    @ApiOperation( "添加流程模型")
    public ModelRepresentation modelInsert(@RequestBody FlowModelDTO req){
        return modelFlowService.modelInsert(req);
    }
    /**
     * 导出流程模型的XML
     * @param id 模型id
     * @param response  响应
     */
    @GetMapping("/export-model-xml")
    @ApiOperation( "导出流程模型的XML")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "模型id")
    })
    public void exportModelXML(String id, HttpServletResponse response) throws IOException{
        modelFlowService.exportModelXML(id, response);
    }

    /**
     * 修改流程模型
     * @param dto
     */
    @PostMapping("/model-update")
    @ApiOperation( "修改流程模型")
    public Boolean updateModel(@RequestBody  ModelUpdateDTO dto) throws JsonProcessingException{
       return modelFlowService.updateModel(dto);
    }

    @PostMapping("/import-model")
    @ApiOperation( "导入模型")
    public Boolean importModel(@RequestParam("file") MultipartFile file) throws JsonProcessingException{
        return modelFlowService.importModel(file);
    }

    /**
     * 部署模型模型
     * @param id
     * @return
     */
    @PostMapping("/deploy")
    @ApiOperation( "部署模型模型")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "模型id")
    })
    public Boolean deploy(@RequestParam("id") String id){
        return modelFlowService.deploy(id);
    }

}
