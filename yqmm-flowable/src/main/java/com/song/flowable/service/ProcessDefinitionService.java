package com.song.flowable.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.song.flowable.dto.ProcessDefinitionPageDTO;
import com.song.flowable.dto.StartFlowableDTO;
import com.song.flowable.vo.ProcessDefinitionVO;
import org.flowable.engine.runtime.Execution;

import java.util.List;

/**
 * 功能描述：流程定义
 *
 * @author Songxianyang
 * @date 2022-02-05 22:18
 */
public interface ProcessDefinitionService {

    /**
     * 获得流程定义分页
     *
     * @param dto 分页入参
     * @return 流程定义 Page
     */
    Page<ProcessDefinitionVO> getProcessDefinitionPage(ProcessDefinitionPageDTO dto);


    /**
     * 获得流程定义对应的 BPMN XML
     *
     * @param id 流程定义编号
     * @return BPMN XML
     */
    String getXML(String id);

    /**
     * 启动流程
     * @param dto
     * @return
     */
    String startFlowable(StartFlowableDTO dto);

    /**
     * 查询所有启动的流程列表
     * @param dto
     * @return
     */
    List<Execution> executions(StartFlowableDTO dto);

    /**
     * 挂起流程定义(停止、暂停）或（唤醒、激活）被挂起的流程定义
     * @param definitionId
     * @param state
     * @return
     */
    Boolean stopOrActivate(String definitionId, String state);
}
