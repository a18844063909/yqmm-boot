/*******************************************************************************
 * Package: com.song.flowable.service
 * Type:    ModelService
 * Date:    2023-11-27 20:59
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.song.flowable.dto.FlowModelDTO;
import com.song.flowable.dto.ModelPageReqDTO;
import com.song.flowable.dto.ModelUpdateDTO;
import com.song.flowable.vo.ModelVO;
import org.flowable.ui.modeler.model.ModelRepresentation;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 功能描述：Flowable流程模型
 *
 * @author Songxianyang
 * @date 2023-11-27 20:59
 */

public interface ModelFlowService {
    /**
     * 获得流程模型分页
     *
     * @param dto 分页查询
     * @return Model
     */
    List<ModelVO> getModelPage(ModelPageReqDTO dto);

    /**
     * 删除模型
     *
     * @param id 编号
     */
    Boolean deleteModel(String id);

    /**
     * 修改模型部署的流程定义状态
     *
     * @param id 编号 （1激活 2停止、挂起、暂停）
     * @param state 状态
     */
    String  updateModelState(String id, Integer state);
    /**
     * 获得流程模块
     *
     * @param id 编号
     * @return ModelRespDTO
     */
    ModelVO getModel(String id);

    /**
     * 添加流程模型
     *
     * @param req
     */
    public ModelRepresentation modelInsert(FlowModelDTO req);


    /**
     * 导出流程模型的XML
     * @param id 模型id
     * @param response  响应
     */

    public void exportModelXML(String id, HttpServletResponse response) throws IOException;

    /**
     * 修改流程模型
     * @param dto
     */
    public Boolean updateModel( ModelUpdateDTO dto) throws JsonProcessingException ;

    /**
     * 导入模型
     * @param file
     * @return
     */
    Boolean importModel(MultipartFile file);

    /**
     * 部署模型模型
     * @param id
     * @return
     */
    Boolean deploy(String id);
}
