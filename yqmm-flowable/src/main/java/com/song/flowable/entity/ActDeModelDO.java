package com.song.flowable.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.util.Date;


@Data
@TableName("act_de_model")
public class ActDeModelDO {
	// 模型ID
	private String id;
	// 模型名称
	private String name;
	// 模型键
	private String modelKey;
	// 模型描述
	private String description;
	// 模型注释
	private String modelComment;
	// 创建时间
	private Date created;
	// 创建者
	private String createdBy;
	// 最后更新时间
	private Date lastUpdated;
	// 最后更新者
	private String lastUpdatedBy;
	// 版本号
	private Integer version;
	// 模型编辑器JSON
	private String modelEditorJson;
	// 缩略图
	private Byte[] thumbnail;
	// 模型类型
	private Integer modelType;
	// 租户ID
	private String tenantId;
}