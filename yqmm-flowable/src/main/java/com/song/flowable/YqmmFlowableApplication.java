package com.song.flowable;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@MapperScan("com.song.flowable.mapper")
@ComponentScan(basePackages = {"com.song.common","com.song.flowable"})
public class YqmmFlowableApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(YqmmFlowableApplication.class, args);
    }
    
}
