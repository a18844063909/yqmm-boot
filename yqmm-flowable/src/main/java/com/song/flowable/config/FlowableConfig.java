/*******************************************************************************
 * Package: com.song.flowable.config
 * Type:    FlowableConfig
 * Date:    2022-02-05 22:41
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.config;

import com.song.flowable.util.MyIdGenerator;
import org.flowable.engine.ProcessEngine;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述：工作流配置类
 *
 * @author Songxianyang
 * @date 2022-02-05 22:41
 */
@Configuration
public class FlowableConfig {
    /**
     * 解决flowable图片中的中文乱码
     * @param configuration
     * @return
     */
    @Bean
    public ProcessEngine processEngine(SpringProcessEngineConfiguration configuration) {
        configuration.setActivityFontName("宋体");
        configuration.setLabelFontName("宋体");
        configuration.setAnnotationFontName("宋体");
        // 修改自动生成id
        configuration.setIdGenerator(new MyIdGenerator());
        return configuration.buildProcessEngine();
    }
}
