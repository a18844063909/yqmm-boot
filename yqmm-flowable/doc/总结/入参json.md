通过或者拒绝
```json
{
  "map":{
    "startUserId":"admin"
  },
  "message":"通过",
  "taskId":"96e0601ae8ad45eea3cbe01dcc15ed97",
  "processInstanceId":"51e3eb61b54a440e9b61c63650e69e12",
  "userId":"admin"
}
```
批量删除流程实例
```json
{
	"processInstanceIds": [],
	"reason": ""
}
```
用户待办
```json
{
	"page": 0,
	"pageSize": 1000,
	"userId": "admin"
}
```
# 流程定义
列表分页
```json
{
  "key": "dsqbjsj-key",
  "page": 0,
  "pageSize": 1000
}
```
添加流程模型
```json
{
"description": "test00001",
"key": "flow_test00001",
"name": "test00001"
}
```

修改流程模型
```json
{
    "id":"ff681875-998b-11ee-880a-dc41a90b0909",
    "name":"我的test流程",
    "key":"wwwww1111",
	"description": "终于成功了：修改流程模型",
    "version":0,
    "createName":"admin",
    "createTime":"2023-12-13T07:48:25.908+00:00",
    "lastUpdateTime":"2023-12-13T07:48:25.908+00:00",
    "lastUpdatedName":"admin",
    "publishState":0,
    "flowXml":"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://www.flowable.org/processdef\" exporter=\"Flowable Open Source Modeler\" exporterVersion=\"6.8.0\">\n  <process id=\"wwwww1111\" name=\"wwwww1111\" isExecutable=\"true\">\n    <documentation>wwwww1111</documentation>\n    <startEvent id=\"startEvent1\" flowable:formFieldValidation=\"true\"></startEvent>\n    <userTask id=\"sid-6F02FBEC-632E-4D02-BBCC-DDB0D2192A22\" name=\"弟弟\" flowable:formFieldValidation=\"true\"></userTask>\n    <sequenceFlow id=\"sid-501CB1EC-3777-422E-AA63-F05EFC497F37\" sourceRef=\"startEvent1\" targetRef=\"sid-6F02FBEC-632E-4D02-BBCC-DDB0D2192A22\"></sequenceFlow>\n    <userTask id=\"sid-D7B5E6E4-ACD7-4888-AD18-E435848872B1\" name=\"妹妹\" flowable:formFieldValidation=\"true\"></userTask>\n    <sequenceFlow id=\"sid-14C77B64-22B8-47A1-927F-5C1A61581EE9\" sourceRef=\"sid-6F02FBEC-632E-4D02-BBCC-DDB0D2192A22\" targetRef=\"sid-D7B5E6E4-ACD7-4888-AD18-E435848872B1\"></sequenceFlow>\n    <endEvent id=\"sid-8D39C17A-33AF-4F35-87DF-0342081603AB\"></endEvent>\n    <sequenceFlow id=\"sid-FE0D83C7-B809-4AF8-8EFA-8C0DF915F0AF\" sourceRef=\"sid-D7B5E6E4-ACD7-4888-AD18-E435848872B1\" targetRef=\"sid-8D39C17A-33AF-4F35-87DF-0342081603AB\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_wwwww1111\">\n    <bpmndi:BPMNPlane bpmnElement=\"wwwww1111\" id=\"BPMNPlane_wwwww1111\">\n      <bpmndi:BPMNShape bpmnElement=\"startEvent1\" id=\"BPMNShape_startEvent1\">\n        <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"100.0\" y=\"163.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"sid-6F02FBEC-632E-4D02-BBCC-DDB0D2192A22\" id=\"BPMNShape_sid-6F02FBEC-632E-4D02-BBCC-DDB0D2192A22\">\n        <omgdc:Bounds height=\"80.0\" width=\"99.99999999999997\" x=\"174.99999739229682\" y=\"137.99999794363978\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"sid-D7B5E6E4-ACD7-4888-AD18-E435848872B1\" id=\"BPMNShape_sid-D7B5E6E4-ACD7-4888-AD18-E435848872B1\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"320.0\" y=\"138.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"sid-8D39C17A-33AF-4F35-87DF-0342081603AB\" id=\"BPMNShape_sid-8D39C17A-33AF-4F35-87DF-0342081603AB\">\n        <omgdc:Bounds height=\"28.0\" width=\"28.0\" x=\"465.0\" y=\"164.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"sid-14C77B64-22B8-47A1-927F-5C1A61581EE9\" id=\"BPMNEdge_sid-14C77B64-22B8-47A1-927F-5C1A61581EE9\" flowable:sourceDockerX=\"49.999999999999986\" flowable:sourceDockerY=\"40.0\" flowable:targetDockerX=\"50.0\" flowable:targetDockerY=\"40.0\">\n        <omgdi:waypoint x=\"274.94999739229684\" y=\"177.99999865202042\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"319.9999999999451\" y=\"177.9999992909103\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"sid-501CB1EC-3777-422E-AA63-F05EFC497F37\" id=\"BPMNEdge_sid-501CB1EC-3777-422E-AA63-F05EFC497F37\" flowable:sourceDockerX=\"15.0\" flowable:sourceDockerY=\"15.0\" flowable:targetDockerX=\"49.999999999999986\" flowable:targetDockerY=\"40.0\">\n        <omgdi:waypoint x=\"129.94999849008173\" y=\"177.99999971958724\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"174.99999739229682\" y=\"177.9999988774143\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"sid-FE0D83C7-B809-4AF8-8EFA-8C0DF915F0AF\" id=\"BPMNEdge_sid-FE0D83C7-B809-4AF8-8EFA-8C0DF915F0AF\" flowable:sourceDockerX=\"50.0\" flowable:sourceDockerY=\"40.0\" flowable:targetDockerX=\"14.0\" flowable:targetDockerY=\"14.0\">\n        <omgdi:waypoint x=\"419.95000000000005\" y=\"178.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"465.0\" y=\"178.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>"
}
```

列表分页 /flow/model/page
```json
{
	"key": "",
	"page": 0,
	"pageSize": 12220
}
```


```json

```


```json

```


```json

```


```json

```

```json

```

```json

```
```json

```
```json

```
```json

```

```json

```


