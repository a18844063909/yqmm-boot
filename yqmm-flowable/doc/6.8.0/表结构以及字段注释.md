[Flowable中文手册](https://tkjohn.github.io/flowable-userguide/#flowableUIApps "Flowable中文手册")

## 一、**flowable表结构**

**ACT\_RE\_**\* : ’RE’表示 repository（存储）。RepositoryService 接口操作的表。带此前缀的表包含的是静态信息，如，流程定义，流程的资源（图片，规则等）。

**ACT\_RU\_**\* : ’RU’表示 runtime。这是运行时的表存储着流程变量，用户任务，变量，职责（job）等运行时的数据。flowable 只存储实例执行期间的运行时数据，当流程实例结束时，将删除这些记录。这就保证了这些运行时的表小且快。

**ACT\_ID\_**\* : ’ID’表示 identity (组织机构)。这些表包含标识的信息，如用户，用户组，等等。

**ACT\_HI\_**\* : ’HI’表示 history。就是这些表包含着历史的相关数据，如结束的流程实例，变量，任务，等等。

**ACT\_GE\_**\* : 普通数据，各种情况都使用的数据。

#### 二、数据库表结构 (34 张表，不同版本数量可能会有出入)

**一般数据 (2)**

ACT\_GE\_BYTEARRAY 通用的流程定义和流程资源  
ACT\_GE\_PROPERTY 系统相关属性

**流程历史记录 (8)**

ACT\_HI\_ACTINST 历史的流程实例  
ACT\_HI\_ATTACHMENT 历史的流程附件  
ACT\_HI\_COMMENT 历史的说明性信息  
ACT\_HI\_DETAIL 历史的流程运行中的细节信息  
ACT\_HI\_IDENTITYLINK 历史的流程运行过程中用户关系  
ACT\_HI\_PROCINST 历史的流程实例  
ACT\_HI\_TASKINST 历史的任务实例  
ACT\_HI\_VARINST 历史的流程运行中的变量信息

**用户用户组表 (9)**

ACT\_ID\_BYTEARRAY 二进制数据表  
ACT\_ID\_GROUP 用户组信息表  
ACT\_ID\_INFO 用户信息详情表  
ACT\_ID\_MEMBERSHIP 人与组关系表  
ACT\_ID\_PRIV 权限表  
ACT\_ID\_PRIV\_MAPPING 用户或组权限关系表  
ACT\_ID\_PROPERTY 属性表  
ACT\_ID\_TOKEN 系统登录日志表  
ACT\_ID\_USER 用户表

**流程定义表 (3)**

ACT\_RE\_DEPLOYMENT 部署单元信息  
ACT\_RE\_MODEL 模型信息  
ACT\_RE\_PROCDEF 已部署的流程定义

**运行实例表 (10)**

ACT\_RU\_DEADLETTER\_JOB 正在运行的任务表  
ACT\_RU\_EVENT\_SUBSCR 运行时事件  
ACT\_RU\_EXECUTION 运行时流程执行实例  
ACT\_RU\_HISTORY\_JOB 历史作业表  
ACT\_RU\_IDENTITYLINK 运行时用户关系信息  
ACT\_RU\_JOB 运行时作业表  
ACT\_RU\_SUSPENDED\_JOB 暂停作业表  
ACT\_RU\_TASK 运行时任务表  
ACT\_RU\_TIMER\_JOB 定时作业表  
ACT\_RU\_VARIABLE 运行时变量表

**其他表 (2)**

ACT\_EVT\_LOG 事件日志表  
ACT\_PROCDEF\_INFO 流程定义信息

### 数据库表，不同版本可能会有些许出入：

<table><tbody><tr><td><p><strong><strong>表分类</strong></strong></p></td><td><p><strong><strong>表名</strong></strong></p></td><td><p><strong><strong>注释</strong></strong></p></td></tr><tr><td><p><strong><strong>一般数据（2）</strong></strong></p></td><td></td><td></td></tr><tr><td></td><td><p>ACT_GE_BYTEARRAY</p></td><td><p>通用的流程定义和流程资源（二进制格式）</p></td></tr><tr><td></td><td><p>ACT_GE_PROPERTY</p></td><td><p>系统相关属性</p></td></tr><tr><td><p><strong><strong>流程历史记录 (8)</strong></strong></p></td><td></td><td></td></tr><tr><td></td><td><p>ACT_HI_ACTINST</p></td><td><p>历史的流程实例</p></td></tr><tr><td></td><td><p>ACT_HI_ATTACHMENT</p></td><td><p>历史的流程附件</p></td></tr><tr><td></td><td><p>ACT_HI_COMMENT</p></td><td><p>历史的说明性信息</p></td></tr><tr><td></td><td><p>ACT_HI_DETAIL</p></td><td><p>历史的流程运行中的细节信息</p></td></tr><tr><td></td><td><p>ACT_HI_IDENTITYLINK</p></td><td><p>历史的流程运行过程中用户关系</p></td></tr><tr><td></td><td><p>ACT_HI_PROCINST</p></td><td><p>历史的流程实例</p></td></tr><tr><td></td><td><p>ACT_HI_TASKINST</p></td><td><p>历史的任务实例</p></td></tr><tr><td></td><td><p>ACT_HI_VARINST</p></td><td><p>历史的流程运行中的变量信息</p></td></tr><tr><td><p><strong><strong>用户用户组表 (9)</strong></strong></p></td><td></td><td></td></tr><tr><td></td><td><p>ACT_ID_BYTEARRAY</p></td><td><p>二进制资源数据表</p></td></tr><tr><td></td><td><p>ACT_ID_GROUP</p></td><td><p>用户组信息表</p></td></tr><tr><td></td><td><p>ACT_ID_INFO</p></td><td><p>用户信息详情表</p></td></tr><tr><td></td><td><p>ACT_ID_MEMBERSHIP</p></td><td><p>人与组关系表</p></td></tr><tr><td></td><td><p>ACT_ID_PRIV</p></td><td><p>权限表</p></td></tr><tr><td></td><td><p>ACT_ID_PRIV_MAPPING</p></td><td><p>用户或组权限关系表</p></td></tr><tr><td></td><td><p>ACT_ID_PROPERTY</p></td><td><p>属性表</p></td></tr><tr><td></td><td><p>ACT_ID_TOKEN</p></td><td><p>系统登录日志表</p></td></tr><tr><td></td><td><p>ACT_ID_USER</p></td><td><p>用户表</p></td></tr><tr><td><p><strong><strong>流程定义表 (3)</strong></strong></p></td><td></td><td></td></tr><tr><td></td><td><p>ACT_RE_DEPLOYMENT</p></td><td><p>部署单元信息</p></td></tr><tr><td></td><td><p>ACT_RE_MODEL</p></td><td><p>模型信息</p></td></tr><tr><td></td><td><p>ACT_RE_PROCDEF</p></td><td><p>已部署的流程定义</p></td></tr><tr><td><p><strong><strong>运行实例表 (10)&nbsp;</strong></strong></p></td><td></td><td></td></tr><tr><td></td><td><p>ACT_RU_DEADLETTER_JOB</p></td><td><p>正在运行的任务表</p></td></tr><tr><td></td><td><p>ACT_RU_EVENT_SUBSCR</p></td><td><p>运行时事件</p></td></tr><tr><td></td><td><p>ACT_RU_EXECUTION</p></td><td><p>运行时流程执行实例</p></td></tr><tr><td></td><td><p>ACT_RU_HISTORY_JOB</p></td><td><p>历史作业表</p></td></tr><tr><td></td><td><p>ACT_RU_IDENTITYLINK</p></td><td><p>运行时用户关系信息</p></td></tr><tr><td></td><td><p>ACT_RU_JOB</p></td><td><p>运行时作业表</p></td></tr><tr><td></td><td><p>ACT_RU_SUSPENDED_JOB</p></td><td><p>暂停作业表</p></td></tr><tr><td></td><td><p>ACT_RU_TASK</p></td><td><p>运行时任务表</p></td></tr><tr><td></td><td><p>ACT_RU_TIMER_JOB</p></td><td><p>定时作业表</p></td></tr><tr><td></td><td><p>ACT_RU_VARIABLE</p></td><td><p>运行时变量表</p></td></tr><tr><td><p><strong><strong>其他表 (2)</strong></strong></p></td><td></td><td></td></tr><tr><td></td><td><p>ACT_EVT_LOG</p></td><td><p>事件日志表</p></td></tr><tr><td></td><td><p>ACT_PROCDEF_INFO</p></td><td><p>流程定义信息</p></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr></tbody></table>
DELEGATION_  委托状态
### 数据库表结构描述

#### 1\. ACT\_GE\_BYTEARRAY 资源表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 | Activiti 有可能会被频繁修改数据库表，加入字段，用来表示该数据被操作的次数 |
| NAME\_ | NVARCHAR2(255) | N | 资源名称 |  |
| DEPLOYMENT\_ID\_ | NVARCHAR2(64) | N | 部署序号 | 部署序号，一次部署可以部署多个资源，该字段与部署表 ACT\_RE\_DEPLOYMENT 的主键关联 |
| BYTES\_ | BLOB | N | 资源内容 |  |
| GENERATED\_ | NUMBER(1) | N | 是否是右 activiti 自动产生的资源 | 0 表示 false，1 表示 true |

#### 2\. ACT\_GE\_PROPERTY 属性表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| NAME\_ | NVARCHAR2(64) | Y | 属性名称 |  |
| VALUE\_ | NVARCHAR2(300) | N | 属性值 |  |
| REV\_ | INTEGER | N | 数据版本号 |  |

### 3\. ACT\_RE\_DEPLOYMENT 部署数据表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 部署序号 |  |
| NAME\_ | NVARCHAR2(255) | N | 部署名称 |  |
| CATEGORY\_ | NVARCHAR2(255) | N | 类别 | 流程定义的 Namespace 就是类别 |
| KEY\_ | NVARCHAR2(255) | N | 流程定义 ID |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |
| DEPLOY\_TIME\_ | TIMESTAMP(6) | N | 部署时间 |  |
| ENGINE\_VERSION\_ | NVARCHAR2(255) | N | 引擎版本 |  |

### 4\. ACT\_RE\_PROCDEF 流程定义表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本号 |  |
| CATEGORY\_ | NVARCHAR2(255) | N | 流程定义分类 | 读取 xml 文件中程的 targetNamespace 值 |
| NAME\_ | NVARCHAR2(255) | N | 流程定义的名称 | 读取流程文件中 process 元素的 name 属性 |
| KEY\_ | NVARCHAR2(255) | N | 流程定义 key | 读取流程文件中 process 元素的 id 属性 |
| VERSION\_ | INTEGER | N | 版本 |  |
| DEPLOYMENT\_ID\_ | NVARCHAR2(64) | N | 部署 ID | 流程定义对应的部署数据 ID |
| RESOURCE\_NAME\_ | NVARCHAR2(2000) | N | bpmn 文件名称 | 一般为流程文件的相对路径 |
| DGRM\_RESOURCE\_NAME\_ | VARCHAR2(4000) | N | 流程定义对应的流程图资源名称 |  |
| DESCRIPTION\_ | NVARCHAR2(2000) | N | 说明 |  |
| HAS\_START\_FORM\_KEY\_ | NUMBER(1) | N | 是否存在开始节点 formKey | start 节点是否存在 formKey 0 否 1 是 |
| HAS\_GRAPHICAL\_NOTATION\_ | NUMBER(1) | N |  |  |
| SUSPENSION\_STATE\_ | INTEGER | N | 流程定义状态 | 1 激活、2 中止 |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |
| ENGINE\_VERSION\_ | NVARCHAR2(255) | N |  | 引擎版本 |

### 5\. ACT\_ID\_USER 用户表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| FIRST\_ | NVARCHAR2(255) | N | 人名 |  |
| LAST\_ | NVARCHAR2(255) | N | 姓氏 |  |
| EMAIL\_ | NVARCHAR2(255) | N | 邮件 |  |
| PWD\_ | NVARCHAR2(255) | N | 用户密码 |  |
| PICTURE\_ID\_ | NVARCHAR2(64) | N | 图片 ID |  |

### 6\. ACT\_ID\_INFO 用户信息表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| USER\_ID\_ | NVARCHAR2(64) | N | 对应用户表主键 |  |
| TYPE\_ | NVARCHAR2(64) | N | 信息类型 | 当前可以设置用户帐号 (account)、用户信息 (userinfo) 和 NULL 三种值 |
| KEY\_ | NVARCHAR2(255) | N | 数据的键 | 可以根据该键查找用户信息的值 |
| VALUE\_ | NVARCHAR2(255) | N | 数据的值 |  |
| PASSWORD\_ | BLOB | N | 用户密码 |  |
| PARENT\_ID\_ | NVARCHAR2(255) | N | 父信息 ID |  |

### 7\. ACT\_ID\_GROUP 用户组表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| NAME\_ | NVARCHAR2(255) | N | 用户组名称 |  |
| TYPE\_ | NVARCHAR2(255) | N | 用户组类型 |  |

### 8\. ACT\_ID\_MEMBERSHIP 关系表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| USER\_ID\_ | NVARCHAR2(64) | Y | 用户 ID |  |
| GROUP\_ID\_ | NVARCHAR2(64) | Y | 用户组 ID |  |

### 9\. ACT\_RU\_EXECUTION 流程实例 (执行流) 表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| BUSINESS\_KEY\_ | NVARCHAR2(255) | N | 业务主键 ID |  |
| PARENT\_ID\_ | NVARCHAR2(64) | N | 父执行流的 ID |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 流程定义的数据 ID |  |
| SUPER\_EXEC\_ | NVARCHAR2(64) | N |  |  |
| ROOT\_PROC\_INST\_ID\_ | NVARCHAR2(64) | N |  |  |
| ACT\_ID\_ | NVARCHAR2(255) | N | 节点实例 ID |  |
| IS\_ACTIVE\_ | NUMBER(1) | N | 是否存活 |  |
| IS\_CONCURRENT\_ | NUMBER(1) | N | 执行流是否正在并行 |  |
| IS\_SCOPE\_ | NUMBER(1) | N |  |  |
| IS\_EVENT\_SCOPE\_ | NUMBER(1) | N |  |  |
| IS\_MI\_ROOT\_ | NUMBER(1) | N |  |  |
| SUSPENSION\_STATE\_ | INTEGER | N | 流程终端状态 |  |
| CACHED\_ENT\_STATE\_ | INTEGER | N |  |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |
| NAME\_ | NVARCHAR2(255) | N |  |  |
| START\_TIME\_ | TIMESTAMP(6) | N | 开始时间 |  |
| START\_USER\_ID\_ | NVARCHAR2(255) | N |  |  |
| LOCK\_TIME\_ | TIMESTAMP(6) | N |  |  |
| IS\_COUNT\_ENABLED\_ | NUMBER(1) | N |  |  |
| EVT\_SUBSCR\_COUNT\_ | INTEGER | N |  |  |
| TASK\_COUNT\_ | INTEGER | N |  |  |
| JOB\_COUNT\_ | INTEGER | N |  |  |
| TIMER\_JOB\_COUNT\_ | INTEGER | N |  |  |
| SUSP\_JOB\_COUNT\_ | INTEGER | N |  |  |
| DEADLETTER\_JOB\_COUNT\_ | INTEGER | N |  |  |
| VAR\_COUNT\_ | INTEGER | N |  |  |
| ID\_LINK\_COUNT\_ | INTEGER | N |  |  |

### 10\. ACT\_RU\_TASK 流程任务表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 任务所在的执行流 ID |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 流程定义数据 ID |  |
| NAME\_ | NVARCHAR2(255) | N | 任务名称 |  |
| PARENT\_TASK\_ID\_ | NVARCHAR2(64) | N | 父任务 ID |  |
| DESCRIPTION\_ | NVARCHAR2(2000) | N | 说明 |  |
| TASK\_DEF\_KEY\_ | NVARCHAR2(255) | N | 任务定义的 ID 值 |  |
| OWNER\_ | NVARCHAR2(255) | N | 任务拥有人 |  |
| ASSIGNEE\_ | NVARCHAR2(255) | N | 被指派执行该任务的人 |  |
| DELEGATION\_ | NVARCHAR2(64) | N |  |  |
| PRIORITY\_ | INTEGER | N |  |  |
| CREATE\_TIME\_ | TIMESTAMP(6) | N | 创建时间 |  |
| DUE\_DATE\_ | TIMESTAMP(6) | N | 耗时 |  |
| CATEGORY\_ | NVARCHAR2(255) | N |  |  |
| SUSPENSION\_STATE\_ | INTEGER | N | 是否挂起 | 1 代表激活 2 代表挂起 |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |
| FORM\_KEY\_ | NVARCHAR2(255) | N |  |  |
| CLAIM\_TIME\_ | TIMESTAMP(6) | N |  |  |

### 11\. ACT\_RU\_VARIABLE 流程参数表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| TYPE\_ | NVARCHAR2(255) | N | 参数类型 | 可以是基本的类型，也可以用户自行扩展 |
| NAME\_ | NVARCHAR2(255) | N | 参数名称 |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 参数执行 ID |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| TASK\_ID\_ | NVARCHAR2(64) | N | 任务 ID |  |
| BYTEARRAY\_ID\_ | NVARCHAR2(64) | N | 资源 ID |  |
| DOUBLE\_ | NUMBER(\*,10) | N | 参数为 double，则保存在该字段中 |  |
| LONG\_ | NUMBER(19) | N | 参数为 long，则保存在该字段中 |
| TEXT\_ | NVARCHAR2(2000) | N | 用户保存文本类型的参数值 |  |
| TEXT2\_ | NVARCHAR2(2000) | N | 用户保存文本类型的参数值 |  |

### 12\. ACT\_RU\_IDENTITYLINK 流程身份关系表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| GROUP\_ID\_ | NVARCHAR2(255) | N | 用户组 ID |  |
| TYPE\_ | NVARCHAR2(255) | N | 关系数据类型 | assignee 支配人 (组)、candidate 候选人 (组)、owner 拥有人 |
| USER\_ID\_ | NVARCHAR2(255) | N | 用户 ID |  |
| TASK\_ID\_ | NVARCHAR2(64) | N | 任务 ID |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程定义 ID |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 属性 ID |  |

### 13\. ACT\_RU\_JOB 工作数据表 (一般工作表)

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| TYPE\_ | NVARCHAR2(255) | N | 类型 |  |
| LOCK\_EXP\_TIME\_ | TIMESTAMP(6) | N | 锁定释放时间 |  |
| LOCK\_OWNER\_ | NVARCHAR2(255) | N | 挂起者 |  |
| EXCLUSIVE\_ | NUMBER(1) | N |  |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 执行实例 ID |  |
| PROCESS\_INSTANCE\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 流程定义 ID |  |
| RETRIES\_ | INTEGER | N |  |  |
| EXCEPTION\_STACK\_ID\_ | NVARCHAR2(64) | N | 异常信息 ID |  |
| EXCEPTION\_MSG\_ | NVARCHAR2(2000) | N | 异常信息 |  |
| DUEDATE\_ | TIMESTAMP(6) | N | 到期时间 |  |
| REPEAT\_ | NVARCHAR2(255) | N | 重复 |  |
| HANDLER\_TYPE\_ | NVARCHAR2(255) | N | 处理类型 |  |
| HANDLER\_CFG\_ | NVARCHAR2(2000) | N |  |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |

### 14\. ACT\_RU\_DEADLETTER\_JOB 工作数据表 (无法执行工作表)

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| TYPE\_ | NVARCHAR2(255) | N | 类型 |  |
| EXCLUSIVE\_ | NUMBER(1) | N |  |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 执行实例 ID |  |
| PROCESS\_INSTANCE\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 流程定义 ID |  |
| RETRIES\_ | INTEGER | N |  |  |
| EXCEPTION\_STACK\_ID\_ | NVARCHAR2(64) | N | 异常信息 ID |  |
| EXCEPTION\_MSG\_ | NVARCHAR2(2000) | N | 异常信息 |  |
| DUEDATE\_ | TIMESTAMP(6) | N | 到期时间 |  |
| REPEAT\_ | NVARCHAR2(255) | N | 重复 |  |
| HANDLER\_TYPE\_ | NVARCHAR2(255) | N | 处理类型 |  |
| HANDLER\_CFG\_ | NVARCHAR2(2000) | N |  |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |

### 15\. ACT\_RU\_SUSPENDED\_JOB 工作数据表 (暂停工作表)

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| TYPE\_ | NVARCHAR2(255) | N | 类型 |  |
| EXCLUSIVE\_ | NUMBER(1) | N |  |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 执行实例 ID |  |
| PROCESS\_INSTANCE\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 流程定义 ID |  |
| RETRIES\_ | INTEGER | N |  |  |
| EXCEPTION\_STACK\_ID\_ | NVARCHAR2(64) | N | 异常信息 ID |  |
| EXCEPTION\_MSG\_ | NVARCHAR2(2000) | N | 异常信息 |  |
| DUEDATE\_ | TIMESTAMP(6) | N | 到期时间 |  |
| REPEAT\_ | NVARCHAR2(255) | N | 重复 |  |
| HANDLER\_TYPE\_ | NVARCHAR2(255) | N | 处理类型 |  |
| HANDLER\_CFG\_ | NVARCHAR2(2000) | N |  |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |

### 16\. ACT\_RU\_TIMER\_JOB 工作数据表 (定时工作表)

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| TYPE\_ | NVARCHAR2(255) | N | 类型 |  |
| EXCLUSIVE\_ | NUMBER(1) | N |  |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 执行实例 ID |  |
| PROCESS\_INSTANCE\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 流程定义 ID |  |
| RETRIES\_ | INTEGER | N |  |  |
| EXCEPTION\_STACK\_ID\_ | NVARCHAR2(64) | N | 异常信息 ID |  |
| EXCEPTION\_MSG\_ | NVARCHAR2(2000) | N | 异常信息 |  |
| DUEDATE\_ | TIMESTAMP(6) | N | 到期时间 |  |
| REPEAT\_ | NVARCHAR2(255) | N | 重复 |  |
| HANDLER\_TYPE\_ | NVARCHAR2(255) | N | 处理类型 |  |
| HANDLER\_CFG\_ | NVARCHAR2(2000) | N |  |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |

### 17\. ACT\_RU\_EVENT\_SUBSCR 时间描述表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| EVENT\_TYPE\_ | NVARCHAR2(255) | N | 事件类型 |  |
| EVENT\_NAME\_ | NVARCHAR2(255) | N | 事件名称 |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 指定 ID |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程定义 ID |  |
| ACTIVITY\_ID\_ | NVARCHAR2(64) | N | 具体事件 ID |  |
| CONFIGURATION\_ | NVARCHAR2(255) | N | 事件的配置属性 |  |
| CREATED\_ | TIMESTAMP(6) | N | 创建时间 |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 属性 ID |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |

### 18\. ACT\_HI\_PROCINST 流程实例表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| BUSINESS\_KEY\_ | NVARCHAR2(255) | N | 业务主键 |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 属性 ID |  |
| START\_TIME\_ | TIMESTAMP(6) | N | 开始时间 |  |
| END\_TIME\_ | TIMESTAMP(6) | N | 结束时间 |  |
| DURATION\_ | NUMBER(19) | N | 耗时 |  |
| START\_USER\_ID\_ | NVARCHAR2(255) | N | 起始人 |  |
| START\_ACT\_ID\_ | NVARCHAR2(255) | N | 起始节点 |  |
| END\_ACT\_ID\_ | NVARCHAR2(255) | N | 结束节点 |  |
| SUPER\_PROCESS\_INSTANCE\_ID\_ | NVARCHAR2(64) | N | 父流程实例 ID |  |
| DELETE\_REASON\_ | NVARCHAR2(2000) | N | 删除原因 |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |
| NAME\_ | NVARCHAR2(255) | N | 名称 |  |

### 19\. ACT\_HI\_DETAIL 流程明细表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| TYPE\_ | NVARCHAR2(255) | N | 类型 |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 执行 ID |  |
| TASK\_ID\_ | NVARCHAR2(64) | N | 任务 ID |  |
| ACT\_INST\_ID\_ | NVARCHAR2(64) | N | 节点实例 ID |  |
| NAME\_ | NVARCHAR2(255) | N | 名称 |  |
| VAR\_TYPE\_ | NVARCHAR2(64) | N | 参数类型 |  |
| TIME\_ | TIMESTAMP(6) | N | 时间戳 |  |
| BYTEARRAY\_ID\_ | NVARCHAR2(64) | N | 字节表 ID |  |
| DOUBLE\_ | NUMBER(\*,10) | N | 存储变量类型为 Double |  |
| LONG\_ | NUMBER(19) | N | 存储变量类型为 long |  |
| TEXT\_ | NVARCHAR2(2000) | N | 存储变量值类型为 String |  |
| TEXT2\_ | NVARCHAR2(2000) | N | 此处存储的是 JPA 持久化对象时，才会有值。此值为对象 ID |  |

### 20\. ACT\_HI\_TASKINST 历史任务表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 流程定义 ID |  |
| TASK\_DEF\_KEY\_ | NVARCHAR2(255) | N | 任务定义的 ID 值 |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 执行 ID |  |
| PARENT\_TASK\_ID\_ | NVARCHAR2(64) | N | 父任务 ID |  |
| NAME\_ | NVARCHAR2(255) | N | 名称 |  |
| DESCRIPTION\_ | NVARCHAR2(2000) | N | 说明 |  |
| OWNER\_ | NVARCHAR2(255) | N | 实际签收人 任务的拥有者 | 签收人（默认为空，只有在委托时才有值） |
| ASSIGNEE\_ | NVARCHAR2(255) | N | 被指派执行该任务的人 |  |
| START\_TIME\_ | TIMESTAMP(6) | N | 开始时间 |  |
| CLAIM\_TIME\_ | TIMESTAMP(6) | N | 提醒时间 |  |
| END\_TIME\_ | TIMESTAMP(6) | N | 结束时间 |  |
| DURATION\_ | NUMBER(19) | N | 耗时 |  |
| DELETE\_REASON\_ | NVARCHAR2(2000) | N | 删除原因 |  |
| PRIORITY\_ | INTEGER | N | 优先级别 |  |
| DUE\_DATE\_ | TIMESTAMP(6) | N | 过期时间 |  |
| FORM\_KEY\_ | NVARCHAR2(255) | N | 节点定义的 formkey |  |
| CATEGORY\_ | NVARCHAR2(255) | N | 类别 |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |

### 21\. ACT\_HI\_ACTINST 历史行为表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 流程定义 ID |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 执行 ID |  |
| ACT\_ID\_ | NVARCHAR2(255) | N | 节点实例 ID |  |
| TASK\_ID\_ | NVARCHAR2(64) | N | 任务 ID |  |
| CALL\_PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 调用外部的流程实例 ID |  |
| ACT\_NAME\_ | NVARCHAR2(255) | N | 节点名称 |  |
| ACT\_TYPE\_ | NVARCHAR2(255) | N | 节点类型 |  |
| ASSIGNEE\_ | NVARCHAR2(255) | N | 节点签收人 |  |
| START\_TIME\_ | TIMESTAMP(6) | N | 开始时间 |  |
| END\_TIME\_ | TIMESTAMP(6) | N | 结束时间 |  |
| DURATION\_ | NUMBER(19) | N | 耗时 |  |
| DELETE\_REASON\_ | NVARCHAR2(2000) | N | 删除原因 |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |

### 22\. ACT\_HI\_ATTACHMENT 附件表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| USER\_ID\_ | NVARCHAR2(255) | N | 用户 ID |  |
| NAME\_ | NVARCHAR2(255) | N | 名称 |  |
| DESCRIPTION\_ | NVARCHAR2(2000) | N | 说明 |  |
| TYPE\_ | NVARCHAR2(255) | N | 类型 |  |
| TASK\_ID\_ | NVARCHAR2(64) | N | 任务 ID |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| URL\_ | NVARCHAR2(2000) | N |  |  |
| CONTENT\_ID\_ | NVARCHAR2(64) | N | 字节表的 ID |  |
| TIME\_ | TIMESTAMP(6) | N | 时间 |  |

### 23\. ACT\_HI\_COMMENT 评论表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| TYPE\_ | NVARCHAR2(255) | N | 类型 | 类型：event（事件）、comment（意见） |
| TIME\_ | TIMESTAMP(6) | N | 时间 |  |
| USER\_ID\_ | NVARCHAR2(255) | N | 用户 ID |  |
| TASK\_ID\_ | NVARCHAR2(64) | N | 任务 ID |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| ACTION\_ | NVARCHAR2(255) | N | 行为类型 |  |
| MESSAGE\_ | NVARCHAR2(2000) | N | 信息 | 用于存放流程产生的信息，比如审批意见 |
| FULL\_MSG\_ | BLOB | N | 全部内容 |  |

### 24\. ACT\_RE\_MODEL 流程设计模型部署表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| NAME\_ | NVARCHAR2(255) | N |  |  |
| KEY\_ | NVARCHAR2(255) | N |  |  |
| CATEGORY\_ | NVARCHAR2(255) | N | 分类 |  |
| CREATE\_TIME\_ | TIMESTAMP(6) | N | 创建时间 |  |
| LAST\_UPDATE\_TIME\_ | TIMESTAMP(6) | N | 最后更新时间 |  |
| VERSION\_ | INTEGER | N | 版本 |  |
| META\_INFO\_ | NVARCHAR2(2000) | N | 以 json 格式保存流程定义的信息 |  |
| DEPLOYMENT\_ID\_ | NVARCHAR2(64) | N | 部署 ID |  |
| EDITOR\_SOURCE\_VALUE\_ID\_ | NVARCHAR2(64) | N |  |  |
| EDITOR\_SOURCE\_EXTRA\_VALUE\_ID\_ | NVARCHAR2(64) | N |  |  |
| TENANT\_ID\_ | NVARCHAR2(255) | N |  |  |

### 25\. ACT\_EVT\_LOG 事件日志表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| LOG\_NR\_ | NUMBER(19) | Y | 主键 |  |
| TYPE\_ | NVARCHAR2(64) | N | 类型 |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 流程定义 ID |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 执行 ID |  |
| TASK\_ID\_ | NVARCHAR2(64) | N | 任务 ID |  |
| TIME\_STAMP\_ | TIMESTAMP(6) | N |  |  |
| USER\_ID\_ | NVARCHAR2(255) | N |  |  |
| DATA\_ | BLOB | N |  |  |
| LOCK\_OWNER\_ | NVARCHAR2(255) | N |  |  |
| LOCK\_TIME\_ | TIMESTAMP(6) | N |  |  |
| IS\_PROCESSED\_ | NUMBER(3) | N |  |  |

### 26\. ACT\_PROCDEF\_INFO

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| PROC\_DEF\_ID\_ | NVARCHAR2(64) | N | 流程定义 ID |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| INFO\_JSON\_ID\_ | NVARCHAR2(64) | N |  |  |

### 27\. ACT\_HI\_VARINST 历史变量表历史变量表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |
| EXECUTION\_ID\_ | NVARCHAR2(64) | N | 指定 ID |  |
| TASK\_ID\_ | NVARCHAR2(64) | N | 任务 ID |  |
| NAME\_ | NVARCHAR2(255) | N | 名称 |  |
| VAR\_TYPE\_ | NVARCHAR2(100) | N | 参数类型 |  |
| REV\_ | INTEGER | N | 数据版本 |  |
| BYTEARRAY\_ID\_ | NVARCHAR2(64) | N | 字节表 ID |  |
| DOUBLE\_ | NUMBER(\*,10) | N | 存储 double 类型数据 |  |
| LONG\_ | NUMBER(\*,10) | N | 存储 long 类型数据 |  |
| TEXT\_ | NVARCHAR2(2000) | N |  |  |
| TEXT2\_ | NVARCHAR2(2000) | N |  |  |
| CREATE\_TIME\_ | TIMESTAMP(6)(2000) | N |  |  |
| LAST\_UPDATED\_TIME\_ | TIMESTAMP(6)(2000) | N |  |  |

### 28\. ACT\_HI\_IDENTITYLINK 历史流程人员表

| 字段 | 类型 | 主键 | 说明 | 备注 |
| --- | --- | --- | --- | --- |
| ID\_ | NVARCHAR2(64) | Y | 主键 |  |
| GROUP\_ID\_ | NVARCHAR2(255) | N | 组 ID |  |
| TYPE\_ | NVARCHAR2(255) | N | 类型 |  |
| USER\_ID\_ | NVARCHAR2(255) | N | 用户 ID |  |
| TASK\_ID\_ | NVARCHAR2(64) | N | 任务 ID |  |
| PROC\_INST\_ID\_ | NVARCHAR2(64) | N | 流程实例 ID |  |