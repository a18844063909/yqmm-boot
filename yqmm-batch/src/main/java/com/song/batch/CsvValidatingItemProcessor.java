/*******************************************************************************
 * Package: com.song.batch
 * Type:    CsvValidatingItemProcessor
 * Date:    2024-01-19 22:12
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.batch;

import com.song.batch.entity.ProjectUser;
import org.springframework.batch.item.validator.ValidatingItemProcessor;

import javax.validation.ValidationException;

/**
 * 功能描述：
 *  数据处理只需实现ItemProcessor接口，重写其process方法。方法输入的参数是从ItemReader读取到的数据，返回的数据给ItemWriter
 *  对数据加工 然后并返回
 * @author Songxianyang
 * @date 2024-01-19 22:12
 */
public class CsvValidatingItemProcessor  extends ValidatingItemProcessor<ProjectUser> {

    @Override
    public ProjectUser process(ProjectUser item) throws ValidationException {
        // 需执行super.process (item) 才会调用自定义校验器
        super.process(item);
        // 对数据做简单的处理 将性别装换为中文
        if (item.getSex().equals("1")) {
            item.setSex("男");
        } else {
            item.setSex("女");
        }
        return item;
    }
}