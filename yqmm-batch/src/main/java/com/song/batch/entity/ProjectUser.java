/*******************************************************************************
 * Package: com.song.batch.entity
 * Type:    ProjectUser
 * Date:    2024-01-19 22:08
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.batch.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-01-19 22:08
 */
@Data
@AllArgsConstructor
public class ProjectUser {
    @Size(max = 10, min = 2)
    private String name;

    private Integer age;

    private String sex;

    private String address;

    private Date birthday;
}
