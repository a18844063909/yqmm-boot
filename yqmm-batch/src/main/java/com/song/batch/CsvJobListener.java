/*******************************************************************************
 * Package: com.song.batch
 * Type:    CsvJobListener
 * Date:    2024-01-19 22:08
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.batch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * 功能描述：监听器实现JobExecutionListener接口，并重写其beforeJob、afterJob方法即可
 *
 * @author Songxianyang
 * @date 2024-01-19 22:08
 */
@Slf4j
public class CsvJobListener implements JobExecutionListener {

    private long startTime;

    @Override
    public void beforeJob(JobExecution jobExecution) {
        startTime = System.currentTimeMillis();
        log.info("任务处理开始...");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        long endTime = System.currentTimeMillis();
        log.info("任务处理结束，耗时:{} ms", (endTime - startTime));
    }
}
