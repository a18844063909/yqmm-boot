/*******************************************************************************
 * Package: com.song.batch
 * Type:    CsvLineMapper
 * Date:    2024-01-19 22:11
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.batch;

import com.song.batch.entity.ProjectUser;
import org.springframework.batch.item.file.LineMapper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 功能描述：属性赋值操作  作用域：读
 *
 * @author Songxianyang
 * @date 2024-01-19 22:11
 */
public class CsvLineMapper implements LineMapper<ProjectUser> {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @Override
    public ProjectUser mapLine(String line, int lineNumber) throws Exception {
        String[] lines = line.split(",");
        Date date = sdf.parse(lines[4]);
        return new ProjectUser(lines[0], Integer.valueOf(lines[1]), lines[2], lines[3], date);
    }
}
