-- auto-generated definition
create table project_user
(
    id       int auto_increment comment '主键'
        primary key,
    name     varchar(48) null comment '姓名',
    age      int         null comment '年龄',
    sex      varchar(8)  null comment '性别',
    address  varchar(48) null comment '地址',
    birthday timestamp   null comment '生日'
);