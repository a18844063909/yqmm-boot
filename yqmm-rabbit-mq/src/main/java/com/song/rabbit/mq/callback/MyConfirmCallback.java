/*******************************************************************************
 * Package: com.song.rabbit.mq.callback
 * Type:    MyConfirmCallback
 * Date:    2024-04-22 11:06
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.rabbit.mq.callback;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;


/**
 * 功能描述：回调函数
 *
 * @author Songxianyang
 * @date 2024-04-22 11:06
 */
@Slf4j
@Component
public class MyConfirmCallback implements RabbitTemplate.ConfirmCallback {
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        // 获取成功后回调函数
        log.info("correlationData消息唯一id>>>>>{}",correlationData.getId());
        // 返回的消息数据
        log.info("correlationData返回的消息数据>>>>>{}",correlationData.getReturnedMessage());
        log.info("ack是否抵达成功>>>>>{}",ack);
        log.info("cause失败原因，成功则返回null>>>>>{}",cause);

    }
}
