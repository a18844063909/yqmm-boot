/*******************************************************************************
 * Package: com.song.rabbit.mq.config
 * Type:    RabbitmqConfig
 * Date:    2023-05-14 18:18
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.rabbit.mq.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * 功能描述： 消息转换器配置后：消息将返回的是json
 *  默认：private MessageConverter messageConverter = new SimpleMessageConverter();
 *  我们初始化 Jackson2JsonMessageConverter
 * @author Songxianyang
 * @date 2023-05-14 18:18
 */
@SpringBootConfiguration
public class RabbitmqConfig {
    @Bean
    public MessageConverter messageConverter() {
      return   new Jackson2JsonMessageConverter();
    }
}
