package com.song.rabbit.mq;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableRabbit
public class YqmmRabbitMqApplication {

    public static void main(String[] args) {
        SpringApplication.run(YqmmRabbitMqApplication.class, args);
    }

}
