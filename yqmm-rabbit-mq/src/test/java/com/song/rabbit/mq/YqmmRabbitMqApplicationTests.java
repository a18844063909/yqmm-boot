package com.song.rabbit.mq;

import com.song.rabbit.mq.entity.Cat;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Date;

@SpringBootTest
class YqmmRabbitMqApplicationTests {
    @Resource
    private AmqpAdmin amqpAdmin;
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Test
    void contextLoads() {
    }

    @Test
    void createExchange() {
        // 创建交换机
        // String name, boolean durable, boolean autoDelete
        DirectExchange directExchange = new DirectExchange("yqmm.rabbit.mq.java.exchange", true, false);
        amqpAdmin.declareExchange(directExchange);
        System.out.println("yqmm.rabbit.mq.java.exchange创建交换机");
        // String name, boolean 持久化, boolean 排他, boolean autoDelete
        Queue queue = new Queue("yqmm.rabbit.mq.java.queue", true, false, false);
        amqpAdmin.declareQueue(queue);
        System.out.println("yqmm.rabbit.mq.java.queue创建队列");
    }
    // 绑定
    @Test
    void bind() {
        //String 目的地, DestinationType 目标类型:队列, String 交换机名称, String 路由key,
        //			@Nullable Map<String, Object> 各种参数
        Binding binding = new Binding("yqmm.rabbit.mq.java.queue", Binding.DestinationType.QUEUE,
                "yqmm.rabbit.mq.java.exchange", "song.java",
                null);
        amqpAdmin.declareBinding(binding);
        System.out.println("点对点创建绑定关系");
    }

    @Test
    //消费
    void sendMsg() {
        Cat cat = new Cat(1,"大猫",new Date());
        // 发送消息
        // String exchange, String routingKey, Object message, CorrelationData correlationData
        rabbitTemplate.convertAndSend("yqmm.rabbit.mq.java.exchange","song.java",cat);
    }
}
