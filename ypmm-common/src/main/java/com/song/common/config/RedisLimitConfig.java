/*******************************************************************************
 * Package: com.song.common.config
 * Type:    RedisLimitConfig
 * Date:    2024-01-07 13:23
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;

/**
 * 功能描述： 服务限流配置
 *
 * @author Songxianyang
 * @date 2024-01-07 13:23
 */
@Configuration
public class RedisLimitConfig {
    // 引入限流文件
    @Bean
    public DefaultRedisScript loadRedisScript() {
        DefaultRedisScript redisScript = new DefaultRedisScript();
        redisScript.setLocation(new ClassPathResource("lua/CurrentLimiter.lua"));
        redisScript.setResultType(java.lang.Boolean.class);
        return redisScript;
    }
}
