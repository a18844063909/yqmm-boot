/*******************************************************************************
 * Package: com.song.common.constants
 * Type:    MQConstant
 * Date:    2024-03-30 14:56
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.constants;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-03-30 14:56
 */
public interface MQConstant {
     String USER_TOPIC="user-topic";
     String DOG_TOPIC="dog-topic";
     String CONSUMER_GROUP_DOG="dog-consumer-rocket-group";
}
