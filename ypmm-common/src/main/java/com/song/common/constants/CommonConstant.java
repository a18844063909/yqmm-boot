/*******************************************************************************
 * Package: cn.surveyking.module.flowable.constants
 * Type:    CommonConstant
 * Date:    2023-12-21 13:49
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.constants;

/**
 * 功能描述： flowable公共常量类
 *
 * @author Songxianyang
 * @date 2023-12-21 13:49
 */
public interface CommonConstant {
    String NAME_ASC = "%";

    String BPMN20_XML = "bpmn20.xml";

    String one = "1";

    String two = "2";

}
