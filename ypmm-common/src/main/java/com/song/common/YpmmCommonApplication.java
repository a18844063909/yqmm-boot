package com.song.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.song.common.idworker"})
public class YpmmCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(YpmmCommonApplication.class, args);
    }

}
