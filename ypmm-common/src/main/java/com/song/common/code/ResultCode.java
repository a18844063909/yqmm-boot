/*******************************************************************************
 * Package: com.song.common.code
 * Type:    ResultCode
 * Date:    2022-10-04 17:41
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.code;

import java.io.Serializable;

/**
 * 功能描述： 结果码 枚举
 *
 * @author Songxianyang
 * @date 2022-10-04 17:41
 */
public enum ResultCode implements Serializable {


    /** 操作成功 */
    SUCCESS(200, "成功"),

    /** 操作失败 */
    FAIL(500, "操作失败，请联系管理员！"),

    /** 空指针异常 */
    NullPointerException(500, "空指针异常"),

    /** token失效 */
    TokenExpire(500,"token失效 ");

    private Integer code;
    private String msg;
    ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
