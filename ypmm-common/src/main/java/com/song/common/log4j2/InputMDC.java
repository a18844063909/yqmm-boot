/*******************************************************************************
 * Package: com.song.common.log4j2
 * Type:    InputMDC
 * Date:    2024-01-11 22:37
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.log4j2;

import org.slf4j.MDC;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * 功能描述： 用以获取log中的[%X{hostName}]、[%X{ip}]、[%X{applicationName}]三个字段值
 *
 * @author Songxianyang
 * @date 2024-01-11 22:37
 */
@Component
public class InputMDC implements EnvironmentAware {

    private static Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        InputMDC.environment = environment;
    }

    public static void putMDC() {
        MDC.put("hostName", NetUtil.getLocalHostName());
        MDC.put("ip", NetUtil.getLocalIp());
        MDC.put("applicationName", environment.getProperty("spring.application.name"));
    }

}